﻿# Threading in C#
http://www.albahari.com/threading

# Part 1: Getting started

## Esipuhe
- Prosessorien kellotaajuuksien kasvu on hidastunut.
- Uudempi prosessori ei kasvata yksisäikeiden sovelluksen nopeutta merkittävästi.
- Suorituskykyä lisätty kasvattamalla ydinten määrää prosessorin sisällä
  - Mahdollisuus rinnakkaiselle suoritukselle lisääntynyt.

## Johdatus ja konseptit

### Säie (Thread)
- C# tukee rinnakkaista koodin suoritusta monisäikeistyksen avulla.
- Säie on itsepäinen suorituspolku, joka suoriutuu muiden säikeiden rinnalla.
- *Katso esim. 101. Write X and Y*
![alt text](http://www.albahari.com/threading/NewThread.png)
- Säikeen suorituksen alaisuuden voi tarkistaa sen **IsAlive**-ominaisuudesta, jolla on arvo **true**, kun säie on käynnissä.
- Suorituksen jälkeen säiettä ei voi uudelleenkäynnistää.
- Säikeellä on oma muistipino, johon paikalliset muuttujat tallentuvat.
- *Esim 102. Print cycles*
- Säikeet jakavat dataa, jos niillä referensssi samaan objektiin.
- *Esim 103. Share instance*
- Molemmat säikeet jakaavat luokan muuttujan **_done**, joten esimerkki tulostaa vain kerran *Done*.
- Säikeet voivat jakaa dataa myös staattisten muuttujien kautta.
- *Esim 104. Share static member*

### Säieturvallisuus (Thread-safety)
- Kaksi edellistä esimerkkiä havainnollistaa säieturvallisuuden puutetta. Kaksi säiettä käyttää samaan aikaan samaa
muuttujaa, eikä suoritusjärjestystä voi sanoa varmaksi.
- *Kokeile ottaa esimerkin 104 Thread.Sleep pois kommenteista.*
- Jos **Thread.Sleep** saa sovelluksen toimimaan eri tavalla, on logiikassa lähes varmasti bugi.
- **Jaettu data on yleisin syy säieohjelmoinnin kompleksisuuteen ja virheisiin.**

### Lukko (lock)
- Aikaisemman esimerkin ongelma on, että molemmat säikeet voivat ehtiä ehtolausekkeen sisälle, kun
  toinen säie kirjoittaa konsoliin **WriteLine**-metodilla, samalla kun toinen evaluoi ehtolauseketta,
  ennen kuin **_done**-muuttujan arvoksi on asetettu **true**.
- Yleinen lääke samanaikaisuuden hallintaan on eksklusiivinen (pois sulkeva) lukko.
	- Lukko takaa, että vain yksi säie voi suorittaa kerrallaan lukon sisällä olevaa lohkoa.
	- Toinen säie odottaa suoritusvuoroaan, kun toinen säie on lukon sisällä. Estetty (**blocked**) säie ei kuluta CPU-resursseja.
- *Esim 105. Lock*
- Koodista, jossa mahdolliset eri suorituspolut on poistettu lukkojen avulla, voidaan käyttää ilmaisua säieturvallinen (**thread-safe**)
  - Huom! Kahden säieturvallisen metodin kutsuminen peräkkäin ei takaa, että logiikka toimii oikein! Esim. 
    ```
    if(!list.Contains(obj)
    {
        list.Add(obj);
    }
    ```

### Join and Sleep
- Säikeen suorituksen loppumista voi odottaa **Join**-metodilla.
- **Sleep**-metodilla säikeen voi laittaa nukkumaan halutuksi ajaksi.
  - Jos Sleeppiä joutuu käyttämään tuotantokoodissa, on säieturvallisuuden suunnittelu mennyt pieleen.
  - Metodi on kuitenkin hyödyllinen kokeiluissa ja esimerkeissä.
- *Esim. 106 Join and sleep*
- **Thread.Sleep(0)** ja **Thread.Yield()** metodeilla voidaan luovuttaa säieen ajovuoro toiselle säikeelle.
  Yield luovuttaa ajovuoron vain saman prosessorin säikeille.
- Kun säie odottaa vuoroaan Sleepin tai Joinin vuoksi, se ei kuluta CPU-resursseja.

### Säikeistyksen toiminta
- Säikeistys toimii sisäisesti käyttöjärjestelmän tarjoaman säie-vuorontajan (skeduleri, **thread scheduler**) avulla.
- Skedulerin vastuulla on antaa jokaiselle säikeelle ajovuoro ja hoitaa, etteivät odottavat säikeet tuhlaa CPU-resursseja.
- Yksiytimisellä prosessorilla skeduleri suorittaa aikasiivutusta (time-slicing), eli jokaiselle säikeelle annetaan suoritusaikaa nopeilla vaihdoilla.
- Moniydinprosessoreilla saadaan aikaan aito moniajo, jolloin aikasiivutuksen lisäksi ajossa on useampi säie samaan aikaan.
- Tapahtumaa, jossa säikeen suoritus keskeytetään aikasiivutuksen vuoksi, kutsutaan ennaltaehkäisyksi (**preempt**).

### Säie vs Prosessi
- Samalla tavalla kuin prosessit voivat suoriutua rinnakkain käyttöjärjestelmässä,
myös säikeet voivat suoriutua rinnakkain yhden prosessin sisällä.
- Ero on, että prosessit ovat täysin erillään toisistan, kun taas saman prosessin säikeet voivat jakaa dataa kekomuistin (**heap memory**) kautta.
- Tästä syystä säikeet ovat erittäin hyödyllisiä: toinen säie voi hakea dataa samalla kuin toinen näyttää sitä.
- Monisäikeistys kannattaa toteuttaa uudelleenkäytettäviin luokkiin, joita voidaan tutkia ja testata erikseen.
- Useat säikeet eivät aina paranna suorituskykyä. Esimerkiksi 2 I/O säiettä voi toimia paljon nopeammin, kuin 10.
- Myös skedulerin suorittama säikeiden luonti, (kontekstin)vaihto ja tuhoaminen kuluttaa resursseja.

## Säikeiden luonti ja parametrisointi
- Säikeita voidaan luoda **Thread**-luokan avulla.
- Parametreja voidaan välittää eri tavoin.
- Ole tarkkana, että säikeelle välitetty arvo ei ole viite toisen säikeen muuttujaan!
- *Esim. 107 Captured variables*

### Säikeiden nimeäminen
- Säikeille voidaan (ja on suositeltavaa!) antaa nimi.
- Nimi nähdään debuggerissa, joka helpottaa logiikan selvittelyä
- *Esim. 108 Naming threads*

### Etuala- ja taustasäikeet (Foreground and background threads)
- Oletuksena säie on etualan säie, joka pitää sovelluksen elossa, kunnes suoritus loppuu.
- Kun kaikkien etualan säikeiden suoritus loppuu, sovellus sulkeutuu, riippumatta kuinka monta taustasäiettä on elossa/ajossa.
  - **Huom!** Taustasäikeiden finally-lohkoa ei suoriteta tässä tilanteessa!
- *Esim. 109 Background thread*
- Foreground/background-määritys ei vaikuta säikeen prioriteetttiin.

### Säikeen prioriteetti
- Prioriteetti kertoo kuinka paljon suoritusaikaa säie saa suhteessa toisiin säikeisiin.
- Mahdolliset arvot ovat:
  - **Lowest**
  - **BelowNormal**
  - **Normal**
  - **AboveNormal**
  - **Highest**
- Säikeen suoritusta ei voi asettaa reaaliaikaiseksi, ilman että prosessin prioriteetti on reaaliaikainen.
Tämän saavuttamiseksi täytyy asettaa prosessin **PriorityClass**-ominaisuus arvoon **ProcessPriorityClass.High** tai **ProcessPriorityClass.Realtime**.
- Realtime-prioriteetin käyttäminen voi jumittaa pahimmassa tapauksessa koko käyttöjärjestelmän.

### Säikeen poikkeuskäsittely
- Poikkeuskäsittely tulee tehdä aina säikeen sisällä.
- Säikeen ulkopuolella oleva **try-catch**-lohko ei vaikuta säikeen sisälle millään tavoin,
koska säikeellä on täysin oma suorituspolku.
- *Esim. 110 Exception handling*

## Säieallas (Thread Pool)
- Jokaisen säikeen alustus vie muutamia satoja mikrosekunteja ja säie kuluttaa noin 1 megatavun muistia.
- Thread pool vähentää resurssien käyttöä jakamalla ja kierrättämällä olemassa olevia säikeitä.
- Thread pool pitää myös kirjaa säikeiden lukumäärästä ja hallitsee samanaikaisten säikeiden suoritusmäärää.
- Jos liian monta työtä yritetään suorittaa samanaikaisesti, työt jonoontuvat ja odottavat, kunnes thread pool antaa niille säikeen.
- Thread poolin säikeiden määrää voidaan konfiguroida.
- Thread poolia voidaan käyttää eri tavoin:
  - Task Parallel Library (TPL, **Task.Run**, **Task.Factory.StartNew**)
  - **ThreadPool.QueueUserWorkItem**
  - Via asynchronous delegates
  - Via BackgroundWorker
- *Esim. 111 Thread pool via TPL*

# Part 2: Basic synchronization

## Synkronisointi
- Synkronisointi on säikeiden koordinointia, jotta suoritus kulkee aina oletetulla tavalla.
- Erityisen tärkeää, kun säikeet käyttävät jaettua dataa.
- Eri synkronisointitapoja ovat:
  - Blokkaavat metodit (**Sleep**, **Join**, **Task.Wait**)
  - Poissulkevat lukot (**lock**, **Monitor.Enter/Exit**, **Mutex**, **SpinLock**)
  - Ei-poissulkevat lukot (**Semaphore**, **Reader/writer**-lukko)
  - Signalointi (**Monitor.Wait/Pulse**, **CountdownEvent**, **Barrier**)
    - Ei tarvitse pollausta.
  - Ei-blokkaavat käsitteet: (**Thread.MemoryBarrier**, **Thread.VolatileRead**, **Thread.VolatileWrite**, **Interlocked**-luokka)

### Blokkaus (Blocking)
- Säie on estetty/blokattu, kun sen suoritus on unessa (**Sleep**) 
  tai kun se odottaa toiseen säikeen loppumista (**Join**, **EndInvoke**)
- Kun säie blokataan, käyttöjärjestelmä suorittaa kontekstinvaihdon (**Context switch**),
  joka aiheuttaa overheadia muutamia mikrosekunteja.
- Säikeen suoritus voi jatkua neljällä eri tavalla:
 - Blokkaava ehto täyttyy
 - Ehdon aikakatkaisu (timeout) laukeaa.
 - Säie keskeytetään **Thread.Interrupt**-metodilla.
 - Säie abortoidaan **Thread.Abort**-metodilla

### Spinnaus (Spinning)
- Blokkauksen sijaan voidaan eteneminen estää myös pelkällä ehdolla:
  ```
  while (!proceed);
  ```
  Tai 
  ```
  while (DateTime.Now < nextStartTime);
  ```

- Tämä kuitenkin kuluttaa huomattavasti resursseja, koska käyttöjärjestelmä antaa säikeelle mahdollisimman paljon 
  aikaa suorittaa ehtoa yhä uudelleen ja uudelleen.
- Joskus käytetään blokkauksen ja spinnauksen yhdistelmää:
  ```
  while (!proceed) Thread.Sleep(10);
  ```
  Tai jos ollaan asynkronisessa koodissa:
  ```  
  while (!proceed) await Task.Delay(10);
  ```

  Nämä ovat huomattavasti resursseja säästävämpiä kuin pelkkä spinnaus, vaikkakaan eivät kovin elegantteja ratkaisuja.

### Säikeen tilat (ThreadState)

![alt text](http://www.albahari.com/threading/ThreadState.png)

## Lukko

- Yleisimmät lukot ovat **lock (Monitor)** ja **Mutex**, joista lock on huomattavasti nopeampi
- Mutex taas toimii useassa prosessissa.
- Lock tarvitsee synkronoitavan objektin: ```lock (object) { ...``` 
  - Mikä tahansa objekti, jonka kaikki säikeet näkevät, voi toimia synkronoitavana objektina.
  - Pitää olla referenssityyppiä!
  - Voi olla instanssimuuttuja tai staattinen muuttuja.
    - Instanssimuuttuja lukottaa vain saman olion eri säikeiden kesken.
    - Staattinen luokan muuttuja lukottaa kaikkien samantyyppisten olioiden suorituksen eri säikeissä!
  - Synkronoitava objekti voi olla myös se objekti, jota halutaan suojata:
    ```
    class ThreadSafe
    {
      List <string> _list = new List <string>();
 
      void Test()
      {
        lock (_list)
        {
          _list.Add ("Item 1");
          ...
    ```
  - Koko olio voidaan myös lukottaa: ```lock (this) { ...```
  - Myös staattinen luokka voidaan lukottaa: ```lock (typeof(Widget)) { ...```

![alt text](LockingOverheads.png)

### Milloin pitää käyttää lukkoa?
- Aina kun käytetään jaettua, kirjoitettavaa muuttujaa!

### Atomisuus (Atomicity)
- Jos muuttujan arvo luetaan ja kirjoitetaan aina saman lukon sisällä niin, että vain yksi säie
  kerrallaan voi muokata sitä, muuttujaa käytetään atomisesti.
- Atomisen koodin suoritus ei voi keskeytyä toisen säikeen johdosta niin, että siitä seuraisi erilainen tulos.

### Sisäkkäinen lukko (Nested Locking)
- Säie voi lukottaa myös sisäkkäin:
  ```
    lock (locker)
    {
      lock (locker)
      {
        lock (locker)
        {
           // Do something...
        }
      }
    } // Lock is released here.
  ```
  Tai
  ```
  Monitor.Enter (locker); Monitor.Enter (locker);  Monitor.Enter (locker); 
  // Do something...
  Monitor.Exit (locker);  Monitor.Exit (locker);   Monitor.Exit (locker);
  ```
- Lukko avataan vasta kun viimeinen lukitus poistetaan.
- Sama tilanne saadaan aikaan, jos kaksi eri metodia käyttävät samaa lukkoa ja kutsuvat toisiaan.

### Deadlock
- Deadlock tapahtuu, kun kaksi säiettä odottavat toisen säikeen lukkoa,
  eikä kumpikaan suostu luopumaan omasta lukostaan.
- *Esim. 201 Deadlock*
- SQL CLR tunnistaa deadlock-tilanteen ja tuhoamalla toisen säikeen.
- .NET CLR ei tunnnista deadlock-tilannetta.

### Mutex
- Sallii vain yhden säikeen suorittaa lukotettua koodilohkoa.
- Voidaan nimetä, jolloin toimii eri prosesseissa.
- *Esim. 202 Mutex in multiple applications*

### Semaphore
- Toimii kuin "yökerhon portsari": sallii tietyn määrän samanaikaisia säikeitä.
- Kun yksi säie lopettaa lukon käytön, seuraava voi aloittaa.
- Ei ole omistajaa, koska useampi säie voi käyttää samaan aikaan.
- .NET Framework 4.0:n mukana tuli myös **SemaphoreSlim**, joka on suorituskykyisempi,
  mutta toimii vain yhdessä prosessissa. Toisin sanoen, sitä ei voi nimetä.
- *Esim. 203 Semaphore*

## Säieturvallisuus
- Sovellus ja metodi on säieturvallinen, jos sen suoritus toimii samalla tavalla riippumatta säikeiden määrästä.
- Säieturvallisuus saavutetaan usein lukoilla ja vähentämällä samanaikaisia säikeitä.
- Yleiskäyttöisistä luokista tehdään harvoin säieturvallisia, koska
  - Säieturvallisuuden toteuttaminen on usein työlästä.
  - Suorituskyky kärsii.
  - Säieturvallinen tyyppi ei välttämättä tee sovelluksesta säieturvallista.
- Tästä syystä säieturvallisuus yleensä toteutetaan vain tarvittaessa.

### Säieturvallisuus sovelluspalvelimilla
- Miksi seuraavassa esimerkissä **RetrieveUser** ei ole lukon sisällä?
  ```
  static class UserCache
  {
    static Dictionary <int, User> _users = new Dictionary <int, User>();
 
    internal static User GetUser (int id)
    {
      User u = null;
 
      lock (_users)
        if (_users.TryGetValue (id, out u))
          return u;
 
      u = RetrieveUser (id);   // Method to retrieve user from database
      lock (_users) _users [id] = u;
      return u;
    }
  }
  ```
- Käytä mieluummin useita lyhyitä lukkoja, kuin yhtä pitkää, jotta muita säikeitä blokataan mahdollisimman vähän.
- Muista, että lukon hakeminen voi kestää vain nanosekunteja (sekunnin miljardisosa),
  kun taas tietokantahaku yleensä millisekunteja (sekunnin tuhannesosa). Ero siis miljoonakertainen!

### Thread affinity
- WPF ja Windows Forms -sovelluksissa oletetaan vain käyttöliittymäsäikeen kutsuvan käyttöliittymän objekteja/kontrolleja.
- Käyttöliittymäobjektin kutsuminen toisesta säikeestä aiheuttaa poikkeuksen Debug-tilassa.
  Ilman debuggeria poikkeusta ei synny, mutta epävarmuus koodin suorituksessa säilyy!
- Jos säikeestä X halutaan käyttää säikeessä Y luotua kontrollia, tulee pyyntö marshalloida.
- *Katso projekti ThreadAffinity.*

### Muuttumattomat oliot (Immutable Objects)
- Olio on muuttumaton (immutable), jos sen ominaisuuksia ei voi muuttaa ulkoisesti tai sisäisesti.
- Tavanomaisesti ominaisuudet ovat vain luettavissa (readonly) ja asetetaan kerran konstruktorissa.
- Muuttumattomat oliot helpottavat monisäikeistyksessä, koska ne poistavat jaettujen muutettavien olioiden ongelman:
  - Oliota ei voi muuttaa vahingossa samaan aikaa, jos sitä ei voi muuttaa ollenkaan.
- LINQ noudattaa tätä paradigmaa.

## Signalointi ja Event Wait Handle
- Event wait handlea (kahvaa) käytetään signalointiin.
- Signaloinnissa toinen säie odottaa, kunnes se saa ilmoituksen (signaalin) toiselta säikeeltä.
- Event wait handle on yksinkertaisin signalointitapa. Niillä ei ole tekemistä eventtien kanssa.
- Event wait handle -tyyppejä ovat:
  - **AutoResetEvent**
  - **ManualResetEvent**
  - **CountdownEvent**
  - **Barrier**
- EventWaitHandle voidaan nimetä, jolloin se näkyy prosessien yli.

![alt text](SignalingOverheads.png)

### AutoResetEvent
- Toimii kuin lipulla toimiva pyöräovi: lipun syöttäminen päästää yhden henkilön läpi.
- Sana "Auto" nimessä tarkoittaa sitä, että "pyöräovi" resetoituu automaattisesti joka signaloinnilla.
- **WaitOne**-metodi saa säikeen odottamaan vuoroa. **Set**-metodi toisesta säikeestä saa odottajan jatkamaan suoritusta.
- Useamman signaalin lähettäminen ennakkoon ei päästä useampaa säiettä etenemään.
- Aikakatkaisua voidaan käyttää, jos vuoroa ei haluta odottaa ikuisesti. Tällöin **WaitOne** palauttaa **false**.
- *Esim. 204 AutoResetEvent**
  ![alt text](http://www.albahari.com/threading/EventWaitHandle.png)
- AutoResetEventillä voidaan toteuttaa jonoja:
  ![alt text](http://www.albahari.com/threading/TwoWaySignaling.png)
- AutoResetEvent voidaan luoda myös EventWaitHandle-luokan avulla:
  ```
  new EventWaitHandle(false, EventResetMode.AutoReset, "MyHandleName");
  ```
  - Tällä tavalla se voidaan myös nimetä, jolloin se näkyy prosessien yli.

### ManualResetEvent
- Toimii kuin tavallinen portti: on joko auki tai kiinni, eikä ota kantaa sen läpi kulkevien ihmisten määrään.
- **Set**-metodi avaa portin ja päästää rajoittamattoman määrän säikeitä etenemään.
- **Reset** sulkee portin.
- Jos monta säiettä odottaa **WaitOne**-metodin avulla, kaikki säikeet lähtevät etenemään, kun portti avataan.
- Muuten samanlainen kuin AutoResetEvent.
- ManualResetEvent voidaan luoda myös EventWaitHandle-luokan avulla:
  ```
  new EventWaitHandle(false, EventResetMode.AutoReset, "MyHandleName");
  ```
  - Tällä tavalla se voidaan myös nimetä, jolloin se näkyy prosessien yli.

### CountdownEvent
- Toimii kuin portti, joka odottaa, että henkilöt ovat saapuneet paikalle, ennen kuin portti aukeaa.
- Säikeet kutsuvat Signal-metodia ja jäävät odottamaan. Kun kaikki ovat kutsuneet, portti avautuu ja säikeet etenevät.
- *Esim. 206 Countdown**
- Odottettavien säikeiden määrää voidaan lisätä **AddCount**-metodilla, jos **Count** ei ole vielä 0.

### Wait Handles and Thread Pool
- Jos sovelluksessa on paljon säikeitä, jotka ovat usein blokattuja, voi olla tehokkaampaa käyttää
  **ThreadPool.RegisterWaitForSingleObject**-metodia töiden suorittamiseen.
- Metodi ottaa parametriksi delegaatiin, joka suoritetaan kun wait handlelle annetaan signaali tai kun aikakatkaisu laukeaa.
- *Esim. 207 RegisterWaitForSingleObject*
- Jos 100 clienttia kutsuu serveriä ja serveri luo jokaiselle käsittelylle oman säikeen,
  voi 100 säiettä olla jumissa kunnes ne pääsevät suoritukseen. Tällaisessa tilanteessa olisi
  parempi kutsua **RegisterWaitForSingle**-metodia, jolloin suoritus palautuu heti ilman blokkausta.

## WaitAny, WaitAll ja SignalAndWait
- **Set**, **WaitOne** ja **Reset**-metodien lisäksi **WaitHandle**-luokalla on staattisia metodeita, jotka toimivat atomisesti:
  - **WaitAny**: Odottaa minkä tahansa kahvan signaalia.
  - **WaitAll**: Odottaa kaikkien kahvojen signaalia.
  - **SignalAndWait**: Kutsuu ensimmäisen wait handlen Set-metodia ja toisen WAitOne-metodia:
    ```WaitHandle.SignalAndWait (wh1, wh2);```

### Synkronisointikontekstit (Synchronization contexts)
- Manuaalisen lukotuksen sijaan, lukko voidaan julistaa/määrittää luokalle perimällä se ContextBoundObject-luokasta
  ja lisäämällä Synchronization-attribuutti.
- *Esim. 210 Synchronization Attribute

# Part 3: Using Threads

## Event-Based Asynchronous Pattern (EAP)
- Tarjoaa yksinkertaisia tapoja monisäikeistykseen, ilman että kuluttajan tarvitsee eksplisiittisesti luoda
  ja hallita säikeitä.
- Tarjoaa myös 
  - mahdollisuuden keskeyttää työ.
  - turvallisen tavan päivittää käyttöliittymän (WPF, Windows Forms), kun työ on valmis.
  - poikkeuksien välityksen kutsujalle tapahtumien avulla.
- EAP on vain malli (pattern), joka täytyy itse toteuttaa.
- Vain muutama Frameworkin luokka käyttää tätä patternia, esim. **BackgroundWorker** ja **WebClient**
  ![alt text](EapPattern.png)
- EAP on uusien Taskien myötä hieman vanhentunut käsite.
- *Esim. 301 Background worker*
- *Esim. 302 WebClient*

## Interrupt ja Abort
- Kaikki blokkaavat metodit kuten **Sleep**, **Join**, **EndInvoke** ja **Wait** blokkaavat ikuisesti,
  jos blokkaava ehto ei täyty, eikä aikakatkaisua ole määritelty.
- Joskus voi olla hyödyllistä vapauttaa blokattu säie ennenaikaisesti, esim. jos sovellus on sulkeutumassa.
  Tämän voi tehdä metodeilla:
  - **Thread.Interrupt**
  - **Thread.Abort**

### Interrupt
- Interrup-metodia kutsuttaessa, suoritus heittää väkisin poikkeuksen **ThreadInterruptedException**.
- Suoritus säikeessä ei pääty, ellei poikkeusta jätetä ottamatta kiinni.
- Käyttötarkoituksia tälle metodille on hyvin vähän.
- *Esim. 303 Interrupt**

### Abort
- Samantapainen kuin Interrupt-metodi, mutta heittää poikkeuksen **ThreadAbortException**.
- Jos **catch**-lohkossa ei kutsuta **Thread.ResetAbort**-metodia, sama poikkeus heitetään uudelleen catch-lohkon lopussa.
- *Esim. 304 Interrupt**

## Säikeen turvallinen lopetus (Safe Cancellation)
- **Interrupt** ja **Abort** ovat vaarallisia, koska poikkeus heitetään kesken suorituksen.
- Parempi tapa on hallittu lopetus **CancellationTokenSource**-olionn 
  - **Cancel**-metodi pyytää suorituksen lopetusta.
  - **ThrowIfCancellationRequested**-metodi heittää OperationCanceledException-poikkeuksen.
- *Esim. 305 CancellationToken*
- Tietyt luokat tukevat CancellationTokenin käyttöä:
  - ManualResetEventSlim ja SemaphoreSlim
  - CountdownEvent
  - Barrier
  - BlockingCollection
  - PLINQ ja Task Parallel Library

## Laiska alustus (Lazy Initialization)
- Tavanomainen ongelma monisäikeisessä toteutuksessa on jaetun resurssin säieturvallinen laiska alustus.

### Lazy&lt;T&gt;
- Geneerisesti tyypitetty apuluokka laiskoille alustusmetodeille.
- Samanaikaisuuden/turvallisuuden tasoa voidaan muokata parametreilla.
- *Esim. 306 Lazy&lt;T&gt;*.

### LazyInitializer
- Kuin **Lazy&lt;T&gt;**, mutta staattinen luokka. Ei tarvitse erillistä oliota, joten parempi suorituskyky.
- *Esim. 307 LazyInitializer*.

## Thread-Local storage
- Jos muuttujat halutaan eristää toisista säikeistä, se voidaan tehdä **ThreadStatic**-attribuutin avulla.
- *Esim. 309 ThreadStatic (Real threads)**
- *Esim. 308 ThreadStatic (Threadpool)**
- **ThreadStatic** ei tue instanssimuuttujia.

### ThreadLocal&lt;T&gt;
- Geneerinen luokka, jonka avulla voidaan eristää staattisia ja instanssimuuttujia.
- *Esim. 310 ThreadLocal*

### GetData ja SetData
- Kolmas tapa käsitellä säiekohtaista dataa on käyttää **Thread.GetData** ja **Thread.SetData**-metodeita.
- *Esim. 311 LocalDataStoreSlot*

## Timers
- Timerin avulla koodia voidaan suorittaa tietyin väliajoin.
- Timer luokkia on useita:
  - **System.Threading.Timer** (monisäikeinen)
  - **System.Timers.Timer** (monisäikeinen)
  - **System.Windows.Forms.Timer** (Windows Forms timer, yksisäikeinen)
  - **System.Windows.Threading.DispatcherTimer** (WPF timer, yksisäikeinen)
- *Esim. 312 Timer*
- Monisäikeiset timerit käyttävät thread poolia, joten suorittava säie voi vaihtua eri suorituskerroilla.
- Yksisäikeiset timerit eivät käytä thread poolia, vaan ne suoritetaan aina samalla säikeellä, kuin ne on luotu.
  - Ei tarvitse huolehtia säieturvallisuudesta.
  - **Tick** ei laukea ennen kuin edellinen on suoritettu.
  - Käyttöliittymäkontrolleja voidaan kutsua suoraan ilman **Control.Invoke**/**Dispatcher.Invoke**-metodeita.
- Suoritettavaa metodia kutsutaan, vaikka se olisi vielä suorituksessa, joten niiden täytyy olla säieturvallisia.
- Timerin tarkkuus riippuu käyttöjärjestelmästä ja on tyypillisesti noin 10-20 ms alueella.
- Jos tarvitaan tarkempaa timeria, täytyy kutsua suoraan käyttöjärjestelmän natiiveja interop-funktioita (1 ms tarkkuus).

# Part 4: Advanced Threading

## Nonblocking Synchronization
- Lukkojen avulla taataan säieturvallisuus, mutta blokkaava säie aiheuttaa kontekstinvaihtojen myötä overheadia.
- Ei-blokkaavilla synkronisointimenetelmillä voidaan suorittaa yksinkertaisia operaatioita ilman blokkausta, pysäytystä tai odotusta.
- **Huom!** Ei-blokkaavan tai lukottoman monisäikeisen koodin kirjoittaminen on hankalaa ja helppo toteuttaa väärin.
  Mieti tarkkaan kannattaako riski ja todennäköisesti suurempi toteutusaika.

### Memory Barrier ja Volatility
- Kääntäjä, CLR tai CPU voi uudelleenjärjestää koodia parantaakseen suorityskykyä.
- Joskus tällä voi olla sivuvaikutuksia, jotka vaikuttavat suoritukseen:
  ```
  class Foo
  {
    int _answer;
    bool _complete;
 
    void A()
    {
      _answer = 123;
      _complete = true;
    }
 
    void B()
    {
      if (_complete) Console.WriteLine (_answer);
    }
  }
  ```
- Jos metodeita **A** ja **B** suoritetaan samanaikaisesti eri säikeissä, voi olla mahdollista, että joskus tulostuu **"0"**.
- Tämä johtuu siitä mitä aikaisemmin mainittiin:
  - Kääntäjä, CLR tai CPU voi uudelleenjärjestää koodia parantaakseen suorityskykyä.
- *Esim. 401 Compiler reorder*
- Lisää aiheesta: http://igoro.com/archive/volatile-keyword-in-c-memory-model-explained/
- CLR:n optimointi on hyvin tarkka siitä, ettei se riko yksisäikeistä koodia tai monisäikeistä koodia, joka käyttää lukkoja.
  - Muissa tapauksissa turvallisuus täytyy varmistaa itse.  

#### Full Fence
- Yksinkertaisin "muistisuoja" on ns. full memory barrier (full fence), joka estää käskyjen uudelleenjärjestämisen ja
  välimuistin käytön. **Thread.MemoryBarrier**-metodi luo full fencen.
- Korjattu esimerkki:
  ```
  class Foo
  {
    int _answer;
    bool _complete;
 
    void A()
    {
      _answer = 123;
      Thread.MemoryBarrier();    // Barrier 1
      _complete = true;
      Thread.MemoryBarrier();    // Barrier 2
    }
 
    void B()
    {
      Thread.MemoryBarrier();    // Barrier 3
      if (_complete)
      {
        Thread.MemoryBarrier();  // Barrier 4
        Console.WriteLine (_answer);
      }
    }
  }
  ```
- Barrier 1 ja 4 estää suorituksen kirjoittamasta "0".
- Barrier 2 ja 3 varmistaa muuttujien tuoreuden: Jos **B**:tä kutsutaan **A**:n jälkeen, _complete on aina **true**.
- Full fence kestää noin 10 ns.
- MemoryBarrierin käyttö on hankalaa/vaarallista, joten tämän käytölle täytyy olla aika vahvat perusteet.
  - Onko muutaman nanosekunnin parannuksesta hyötyä?
- Välimuistitetun arvon ongelma -esimerkki:
  - *Esim. 402 Compiler optimization* (release build with optimization)
- Seuraavat käyttävät implisittisesti full fencejä:
  - lock (Monitor)
  - Interlocked-luokka
  - Task continuations
  - Signalointikonstruktioiden Set ja Wait-metodit
  - Kaikki mikä perustuu signalointiin, kuten taskin aloitus ja sen odotus.
    - Esim. seuraava on säieturvallinen:
    ```
    int x = 0;
    Task t = Task.Factory.StartNew (() => x++);
    t.Wait();
    Console.WriteLine (x);    // 1
    ```
- Jos esimerkkiin lisätään muuttujia, ei muistisuojia tarvitse lisätä:
  ```
  class Foo
  {
    int _answer1, _answer2, _answer3;
    bool _complete;
 
    void A()
    {
      _answer1 = 1; _answer2 = 2; _answer3 = 3;
      Thread.MemoryBarrier();
      _complete = true;
      Thread.MemoryBarrier();
    }
 
    void B()
    {
      Thread.MemoryBarrier();
      if (_complete)
      {
        Thread.MemoryBarrier();
        Console.WriteLine (_answer1 + _answer2 + _answer3);
      }
    }
  }
   ```

### Volatile
- Älä käytä!
- **Volatile** avainsana opastaa kääntäjää generoimaan puoliaitoja (half-fence): 
  - **acquire-fence**n lisätään jokaiseen lukuoperaatioon ja
  - **release-fence**n lisätään jokaiseen kirjoitusoperaatioon.
- **Acquire-fence** estää toisten luku/kirjoitusoperaatioiden siirtämisen ennen tätä fenceä.
- **Release-fence** estää toisten luku/kirjoitusoperaatioiden siirtämisen fencen jälkeen.
- **Volatile**-avainsanaa ei voi käyttää paikalliseen muuttujaan. Täytyy olla luokkamuuttuja tyyppiä **class** tai **struct**.
- Intelin X86 and X64 prosessorit lisäävät aina **acquire-fence**n lukuoperaatioon ja **release-fence**n kirjoitusoperaatioon,
  joten volatilen käyttö voi olla turhaa. Toisaalta, volatilen käyttö estää kääntäjän ja CLR:n optimoinnin ja vaikuttaa muihin
  prosessorityyppeihin, jotan sen käytöllä on vaikutus.
- Volatile ei takaa takaa säieturvallisuutta kaikissa tilanteissa:
  - Yksi skeenaario mahdollinen, jossa koodin suoritus voi vaihtua:
  ![alt text](Volatile.png)
- Alla olevassa esimerkissä on (teoreettinen) mahdollisuus, että **a** ja **b** ovat nollia suorituksen jälkeen:
  ```
  class IfYouThinkYouUnderstandVolatile
  {
    volatile int x, y;
 
    void Test1()        // Executed on one thread
    {
      x = 1;            // Volatile write (release-fence)
      int a = y;        // Volatile read (acquire-fence)
      ...
    }
 
    void Test2()        // Executed on another thread
    {
      y = 1;            // Volatile write (release-fence)
      int b = x;        // Volatile read (acquire-fence)
      ...
    }
  }
  ```
- Yllä olevan esimerkin vuoksi volatilea tulisi välttää.
  - Disclaimer: Tämä on kirjan kirjoittajan näkemys.
- Volatile-avainsanaa ei voi käyttää pass-by-reference-muuttujiin. Niille voidaan käyttää Thread-luokkaa (tai Volatile-luokkaa):
  ```
  public static void VolatileWrite(ref int address, int value)
  {
    Thread.MemoryBarrier();
    address = value;
  }

  public static int VolatileRead(ref int address)
  {
    int num = address;
    Thread.MemoryBarrier();
    return num;
  }
  ```
- Yllä oleva koodi havainnollistaa samalla aikaisempaa volatile-ongelmaa.
- Lock (Monitor) käyttää full fencejä:
  ```
  lock (someField) { ... }
  ```
  on sama kuin:
  ```
  Thread.MemoryBarrier(); { ... } Thread.MemoryBarrier();
  ```
  jos unohdetaan lockin poissulkevuus.

### Interlocked
- Memory barrier ei riitä, jos luetaan tai kirjoitetaan ilman lukkoja ja operaatio ei ole atominen.
- Esimerkiksi 64-bittisten muuttujien operaatiot 32-bittisissä ympäristöissä suoritetaan kahdessa osassa,
  jolloin operaatio ei ole atominen. Luku toisessa säikeessä kesken kirjoituksen johtaa arvoon, joka on kombinaatio
  vanhasta ja uudesta arvosta (torn read).
- Sama koskee 128-bittisiä lukuja (**decimal**-tyyppi) 64-bittisessä ympäristössä.
  - *Esim. 403 128-bit Torn read*
- Atomisuus esimerkkejä 32-bittisessä ympäristössä:
  ```
  static int _x, _y;
  static long _z;
 
  long myLocal;
  _x = 3;             // Atomic
  _z = 3;             // Nonatomic on 32-bit environs (_z is 64 bits)
  myLocal = _z;       // Nonatomic on 32-bit environs (_z is 64 bits)
  _y += _x;           // Nonatomic (read AND write operation)
  _x++;               // Nonatomic (read AND write operation)
  ```
- **Interlocked**-luokan avulla monista muuttujien sijoituksista ja luvuista voidaan tehdä atomisia.
- *Esim. 404 Interlocked*
- *Esim. 405 CompareExchange*
- **Interlocked**-metodien overhead on noin 10 ns, eli puolet lukon käyttämisestä. Lisäksi ne eivät blokkaa, joten
  ne eivät aiheuta kontekstinvaihtoa.


## Signalointi Wait- ja Pulse-metodeilla
- **Monitor**-luokan **Wait**- ja **Pulse**-metodeilla voi rakentaa oman signalointitoteutuksen.
- Itse asiassa näillä metodeilla ja **lock**illa voi tehdä **AutoResetEvent**, **ManualResetEvent** ja **Semaphore** luokkien toteutuksen ja monia muita.
- Rajoitukset:
  - **Wait/Pulse** eivät vaikuta toisiin prosesseihin.
  - Kaikki signalointiin liittyvät muuttujat täytyy suojata lukolla (lock).
- Suorituskyky noin muutama sata nanosekuntia, noin kolmasosa WantHandlen **Set**-metodista.
- *Esim. 406 Monitor.Wait/Pulse*
- **Monitor.Wait** heittää poikkeuksen, jos sitä ei kutsuta lukon sisältä.
- **Pulse** vapauttaa yhden säikeen. **PulseAll** vapauttaa kaikki.
- **Pulse** tarkoittaa, että jotain on voinut muuttua ja odottava ehto täytyy tarkistaa uudelleen.
  - Ilmoitettava päättää jatkuuko suoritus, ei ilmoittaja.
- Jos **Pulse**a kutsutaan, kun kukaan ei odota, signaali hukataan. Huomaa eroavaisuus **AutoResetEvent**tiin.
  - Pulsen tarkoitus ei siis ole sallia suoritusta, vaan pyytää ilmoitettavaa tarkistamaan voiko hän jatkaa suoritusta.
- **Monitor.Wait**/**Pulse**-metodeilla voi tehdä mitä ihmeellisempiä toteutuksia, esim. producer/consumer-tyyppisen jonon.
- *Esim. 407 Producer/Consumer queue with Monitor*

### Wait timeout
- Monitorin **Wait**-metodille voi antaa timeoutin, jonka jälkeen metodi lopettaa odottamisen ja palauttaa **false**.
- Suorituksen kannalta timeoutin laukeaminen on sama, kuin Monitor olisi saanut Pulssin: lukko varataan uudestaan.
  - Jos lukko on jo käytössä, niin tapahtuu tavanomainen blokkaus ja odotetaan kunnes lukko on käytettävissä ennen sen varausta.
- Timeoutilla voidaan varmistaa, ettei sovellus jää jumiin, jos Wait/Pulse-toteutuksessa on virheitä.
  - **Wait** palauttaa **false**, kun timeout laukeaa. Tämä voi olla hyödyllistä kirjoittaa lokiin virheselvittelyitä varten.

### Two-Way Signaling and Races
- Pulse toimii asynkronisesti (se ei blokkaa ikinä) ja on yksisuuntainen kommunikointi.
- Pulsen kutsuja ei voi tietää tapahtuiko jotain sen ansiosta.
- Kunnollinen **Wait/Pulse**-toteutus vaatii molemminsuuntaisen kommunikaation,
  joten Pulsea-kutsuttaessa on hyvä olla varmistunut, että joku odottaa.
- *Esim. 407 Monitor.Pulse race*

## Barrier
- Barrierin avulla saadaan aikaan säikeiden tapaaminen halutussa kohtaa.
- Barrier alustetaan halutulla määrällä, jota voidaan myöhemmin muuttaa **AddParticipants/RemoveParticipants**-metodeilla.
- Signalointi tapahtuu SignalAndWait-metodilla.
- Kun kaikki osapuolet ovat signaloineet suoritus voi jatkua.
- *Esim. 409 Barrier*
- **PostPhase** on hyödyllinen, kun halutaan tehdä jotain vaiheiden välissä!
  - Barrier ei tarjoa toteutusta datan säilytykseen.

## Reader/Writer lukko
- Aika usein instanssit ovat säieturvallisia samanaikaisille luvuille, mutta ei kirjoitusoperaatiolle.
- Perus lukotus hoitaa tämän ongelman, mutta se blokkaa samanaikaiset lukijat ja suorituskyky kärsii.
- Reader/Writer-lukon avulla lukijoita voi olla useampia, mutta vain yksi kirjoittaja kerrallaan.
  - Lukulukko sallii rinnakkaiset lukijat.
  - Kirjoituslukko estää rinnakkaiset kirjoittajat sekä lukijat.
- Luku toimii EnterReadLock ja ExitReadLock-metodeilla.
- Kirjoitus toimii EnterWriteLock ja ExitWriteLock-metodeilla.
- Lisäksi on TryEnterReadLock ja TryEnterWriteLock.
- *Esim 410 ReaderWriterLockSlim

### Upgradeable Lock
- Joskus tarvitaan ensin lukulukkoa (sisältääkö lista alkion 'a'?) ja sitten kirjoituslukkoa (lisää listalle alkio 'a').
- Tämä saavutetaan päivitettävän lukon avulla: lukulukko muunnetaan kirjoutuslukoksi atomisella operaatiolla.
  - Vain yksi säie saa olla päivitettään lukon sisällä, rinnakkaiset luvut ovat sallittuja.
- *Esim 411 Upgradeable read Lock*  
  
### Lock recursion
- ReaderWriterLockSlim tukee rekursiota, jos se alustetaan LockRecursionPolicy.SupportsRecursion parametrilla.
- Tällöin voidaan kirjoittaa:
  ```
  var rw = new ReaderWriterLockSlim();
  rw.EnterReadLock();
  rw.EnterReadLock();
  rw.ExitReadLock();
  rw.ExitReadLock();
  ```
- Jos rekursiota käytetään ilman **LockRecursionPolicy.SupportsRecursion**-parametria, lentää poikkeus **System.Threading.LockRecursionException**
  - Tällä varmistetaan, että rekursiota todella halutaan käyttää, koska se voi tehdä asiat tarpeettoman hankaliksi.

# Part 5: Parallel programming
- .NET 4.0:ssa tuli monta uutta API-rajapintaa monisäikeistykseen:
  - Parallel LINQ (PLINQ)
  - **Parallel**-luokka
  - **Task**-luokka (Task parallelism)
  - Concurrent collections
  - **SpinLock** ja **SpinWait**
- Yhdessä näitä API-rajapintoja kutsutaan termillä PFX (Parallel Framework)
- Parallel- ja Task-luokka yhdessä kutsutaan Task Parallel Libraryksi (TPL)

## Miksi PFX?
- Prosessorien nopeuksien kasvu on pysähtynyt, mutta ydinten määrä kasvaa: tehoa saa lisää vain samanaikaisella työllä.
- Serverillä rinnakkaisuuden saavuttamien voi olla helppoa, koska clienttien pyynnöt voidaan usein suorittaa rinnakkain.
- Desktopilla rinnakkaisuus on hankalaa, koska yksi suuri tehtävä pitää
  1. Pilkkoa pienempiin osiin
  2. Suorittaa osat yksittäisiä kokonaisuuksina
  3. Koota osien tulokset yhdeksi tulokseksi.
- Myöskään lukotus ei kovin helpota tilannetta, koska lukko estää rinnakkaisen datan muokkaamisen.
- PFX-kirjasto on suunniteltu auttamaan tämänkaltaisessa tilanteessa.

## PFX-konseptit
- On olemassa kaksi strategiaa pilkkoa työ säikeiden kesken:
  - Rinnakkaiden data (structured data parallelism)
    - Data pilkotaan ensin ja sitten käsitellään erikseen eri säikeissä.
    - Yleensä helpompi ja skaalautuu paremmin, koska se ei sisällä jaettua dataa (ei monisäikeistysongelmia)
    - Yleensä alkaa ja loppuu samassa kohtaa koodia.
  - Rinnakkaiden tehtävä (task parallelism)
    - Tehtävät pilkotaan osiin ja suoritetaan eri säikeissä.
    - Yleensä alkaa jossain ja työ hajaantuu koodissa eri paikkoihin.

## PFX-komponentit
![alt text](http://www.albahari.com/threading/ParallelProgramming.png)
- PLINQ tarjoaa rikkaimman toiminnallisuuden: se pilkkoo datan ja kokoaa tulokset automaattisesti.
- Jos LINQ on tuttu, niin käyttö on suhkot helppoa: lisää vain **AsParallel**-metodikutsu.
- *Esim. 501 Prime numbers with PLINQ.*
- Katso "Parallel Execution Ballistics" yllä olevasta esimerkistä.
- Esimerkki, jossa PLINQ:lla muunnetaan kirjaimet isoiksi:
  ![alt text](http://www.albahari.com/threading/PLINQExecution.png)
- Miksei PLINQ ole oletus?
  - Ei takaa tulosjoukon oikeaa järjestystä.
  - Poikkeukset kootaan AggregateException-tyypin sisälle.
  - Virheiden mahdollisuus, jos query sisältää säieturvatonta koodia.
- Datan pilkkomisen ja tuloksien keräämisen tuki eri menetelmillä:
![alt text](ParallelPartitionAndResult.png)

## Tulosjoukon järjestys
- Jos tulosjoukon järjestys on tärkeä, niin **AsParallel**-kutsun jälkeen voidaan kutsua **AsOrdered**-metodia.
- Suorituskyky huononee
- Voidaan kutsua myös **AsUnordered**-metodia, jos tulosjoukon järjestystarve halutaan perua.
  - Hyödyllinen, jos query pitää sisällään monta eri vaihetta datan käsittelyvaihessa ja vain osa vaatii järjestystä.

## PLINQ rajoitteet
- Seuraavat LINQ-metodit eivät tue rinnakkaisuutta, jos data ei ole alkuperäisissä indekseissä (**disclaimer**: tämä voi olla vanhentunutta tietoa):
  - **Take**, **TakeWhile**, **Skip** ja **SkipWhile**
  - Indeksoidut versiot metodeista **Select**, **SelectMany** ja **ElementAt**
- Jotkin PLINQ-metodit käyttävät raskaita partitiointistrategioita:
  - **Join**, **GroupBy**, **GroupJoin**, **Distinct**, **Union**, **Intersect** ja **Except**.
- PLINQ voi päättää olla käyttämättä rinnakkaisuutta, jos se havaitsee siitä olevan enemmän haittaa kuin hyötyä.
  - Voit pakottaa rinnakkaisuuden metodilla **.WithExecutionMode (ParallelExecutionMode.ForceParallelism)**
- *Esim. 502 Parallel spellchecker*

## Functional purity
- Myös PLINQ:n kanssa täytyy muistaa säieturvallisuus!
- *Esim. 503 Functional purity*

## PLINQ ja blokkaavat funktiot
- PLINQ:ä voidaan käyttää myös blokkaavien metodien rinnakkaiseen suoritukseen.
- Metodilla **WithDegreeOfParallelism** voidaan hallita rinnakkaisuuden määrää.
- *Esim. 504 Parallel ping*

## PLINQ Cancellation
- PLINQ tukee CancellationTokenia.
- *Esim. 505 PLINQ Cancellation.*

## PLINQ optimointi
- *Esim. 506 PLINQ Optimization.*

### Output optimization
- Output optimization ForAll-metodilla:
  ![alt text](http://www.albahari.com/threading/ForAll.png)

### Input optimization
- PLINQ-metodit käyttävät kolmea eri strategiaa datan pilkkomisessa:
  ![alt text](PLinqStrategy.png)
- **GroupBy**, **Join**, **GroupJoin**, **Intersect**, **Except**, **Union** ja **Distinct** käyttävät aina **hash partitioning**,
  koska metodit joutuvat vertailemaan.
- Muut metodit käyttävät joko **range** tai **chunk partitioning** menetelmää:
  - Jos input data on indeksoitava (array, IList), valitaan **range partitioning**
  - Muussa tapauksessa valitaan **chunk partitioning**.
- **Range partitioning** voi pakottaa 
  - Vaihda Enumerable.Range muotoon ParallelEnumerable.Range.
  - Tai muuta data indeksoitavaksi **ToArray** tai **ToList**-metodeilla.
- Partitioning:
  ![alt text](http://www.albahari.com/threading/Partitioning.png)

#### Chunk partitioning
  - Alkaa pienillä joukoilla (1-2) ja kasvattaa joukon kokoa:
    - Datan alkupää käsitellään nopeasti, mutta isolla joukolla overhead ei kasva liian isoksi.
  - Hyvää: Säikeet saavat tasaisesti työtä, vaikka niillä kestäisi eri määrä aikaa niiden suorittamiseen.    
  - Huonoa: Säikeet voi blokata, jos datan hakemisessa rinnakkain on esteitä.

#### Range partitioning
  - Valitsee samansuuruisia lohkoja jokaiselle säikeelle.
  - Hyvää: Ei blokkaa säikeitä datan hakemisessa, koska lohkot on ennaltamäärätyt
  - Huonoa: Säikeet ovat idle-tilassa, jos muut säikeet vielä käsittelevät.

### PLINQ ja Aggregate
- Aggregate-metodi tuottaa PLINQ:lle tuskaa.
- Jos metodille ei anna seediä, niin se olettaa, että arvot ovat *vaihdannaisia* ja *liitännäisiä*
  - Toisin sanoen tulokset pitäisi olla sama riippumatta suoritusjärjestyksestä.
- Käytä aina seed factorya Aggregaten kanssa, jos haluat rinnakkaista laskentaa!
- *Esim. 507 PLINQ Aggregate*.

## Parallel-luokka
- Parallel-luokka tarjoaa tuen rinnakkaisajolle kolmen eri staattisen metodin avulla:
  - Parallel.Invoke
    - Suorittaa taulukon delegaatteja rinnakkaisesti.
  - Parallel.For
    - Rinnakkainen for-silmukka.
  - Parallel.ForEach
    - Rinnakkainen foreach-silmukka.
- Kaikki kolme metodia blokkaa, kunnes työ on valmis.
- Kuten PLINQ:ssa, poikkeustilanteessa jäljellä olevat tehtävät pysäytetään nykyisen iteraation jälkeen
  ja poikkeus (tai poikkeukset) heitetään kutsujalle **AggregateException**-tyyppiin sisällytettyinä.
- Tärkeä ero pelkkään **Task.Run**-metodiin:
  - Toimii tehokkaasti isollakin datajoukolla, koska käyttää pinnan alla eri tehtäviin samoja Taskeja,
    jotka käyttävät threadpoolia, joka käyttää samoja säikeitä.
  - Isolla datajoukolla overhead on pienempi.
- *Esim. 508 Parallel class*
- Tuloksien poiminta täytyy hoitaa itse.

### Sisäkkäiset silmukat
- Sisäkkäisistä rinnakkaisista silmukoista saadaan harvoin hyötyä:
  ```
  Parallel.For (0, 100, i =>
  {
    Parallel.For (0, 50, j => Foo (i, j));   // Sequential would be better
  });  
  ```
- Pitäisi olla yli 100 ydintä, että saataisiin hyötyä.

### Indeksoitu Parallel.ForEach
- Parallel.ForEach-tukee myös indeksiä:
  ```
  Parallel.ForEach ("Hello, world", (c, state, i) =>
  {
     Console.WriteLine (c.ToString() + i);
  });
  ```

### ParallelLoopState ja silmukan keskeytys
- Jos seuraava koodi halutaan rinnakkaistaa:
  ```
  foreach (char c in "Hello, world")
    if (c == ',')
      break;
    else
      Console.Write (c);
  ```
- Se tulee kirjoittaa seuraavassa muodossa:
  ```
  Parallel.ForEach ("Hello, world", (c, loopState) =>
  {
    if (c == ',')
      loopState.Break();
    else
      Console.Write (c);
  });

  ```
- ParallelLoopState kertoo mihin iteraatioon jäätiin, jos keskeytys tapahtuu.
- *Esim. 509 ParallelLoopState*
- **ParallelLoopResult.Break** kutsuminen iteraatiossa x varmistaa, että kaikki iteraatiot < x käsitellään.
- **ParallelLoopResult.Stop** kutsuminen pysäyttää kaikki säikeet välittömästi tämänhetkisen iteraation jälkeen,
  jolloin voi käydä niin, että kaikkia iteraatiota, jotka ovat < x ei ole käsitelty.
  - **LowestBreakIteration** on null, jos **Stop**-metodia kutsuttiin.
- Kun **Break**- tai **Stop**-metodia on kutsuttu ja iteraation suoritys kestää pitkään, voidaan suoritys itse keskeyttää
  tarkkailemalla **ParallelLoopResult.ShouldExitCurrentIteration**-ominaisuuden arvoa ja lopettaa tarvittaessa suoritus.
- **ParallelLoopResult.IsExceptional** kertoo onko muissa säikeissä tapahtunut poikkeuksia.

### Paikallisten arvojen optimointi (paluuarvojen keräys)
- **Parallel.For** ja **Parallel.ForEach**-metodeilla on overload, jolla voidaan optimoida helpottaa paluuarvon kertymää.
  ```
  public static ParallelLoopResult For <TLocal> (
    int fromInclusive,
    int toExclusive,
    Func <TLocal> localInit,
    Func <int, ParallelLoopState, TLocal, TLocal> body, 
    Action <TLocal> localFinally);
  ```
- *Esim. 510 Parallel local value optimization*
- Paikallista totaalia käytetään usein ja silloin tällöin se lisätään globaaliin totaaliin.

# Rinnakkaiset tehtävät (Task Parallelism)
- Alimman kerroksen lähestymistapa tehtävien suorittamiseen rinnakkain PFX-kirjaston avulla.
  ![alt text](TaskClasses.png)
- **Task** käyttää thread poolia, jolloin se uudelleenkäyttää säikeitä.
- Parallel ja PLINQ käyttävät Taskeja.
- Taskeilla voidaan 
  - Hienosäätää Taskin skedulointia.
  - Luoda isä-lapsi-suhteita, kun **Task** luodaan toisen sisältä.
  - Toteuttaa hallittu lopetus (cooperative cancellation).
  - Odottaa suorituksen lopetusta ilman signalointia.
  - Jatkosuorittaa tehtäviä, kun toiset loppuvat (task continuation).
  - Ajastaa jatkosuoritus perustuen moniin korreloiviin taskeihin.
  - Välittää poikkeuksia.
- Taskeja voidaan luoda satoja tai tuhansia tehokkaasti, mutta sitä suuremmat määrät olisi hyvä partitioida
  suuremmiksi yksiköiksi, jotta suorituskyky säilyy.

## Taskien luonti
- Taski voidaan luoda eri tavoilla:
  - ```Task.Factory.StartNew(() => Console.WriteLine ("Hello from a task!"));```
    - Sallii lapsi-taskit.
  - ```Task.Run(() => Console.WriteLine ("Hello from a task!"));```
    - Ei salli lapsi-taskeja, vaan niistä tulee detached taskeja.
- Paluuarvon tyyppi voidaan määrittää geneerisellä tyypillä:
  ```
  Task<string> task = Task.Factory.StartNew<string> (() =>    // Begin task
  {
    using (var wc = new System.Net.WebClient())
      return wc.DownloadString ("http://www.linqpad.net");
  });
 
  RunSomeOtherMethod();         // We can do other work in parallel...
 
  string result = task.Result;  // Wait for task to finish and fetch result.
  ```
- **Task** voidaan myös suorittaa synkronisesti metodilla **RunSynchronously**.
- Taski voidaan nimetä välittämällä sille tila.
- *Esim. 511 Creating task*.

### TaskCreationOptions
- Taskia luotaessa, sille voidaan antaa eri optioita:
  - LongRunning
    - Skeduleri antaa taskille oman säikeen, jottei se varaa pitkäksi aikaa lyhyille tehtäville tarkoitettuja taskeja.
    - Hyvä vaihtoehto blokkaaville taskeille.
  - PreferFairness: 
    - Kertoo skedulerille, että taskit tulisi suorittaa siinä järjestyksessä kun on aloitettu.
  - AttachedToParent
    - Kertoo, että kyseessä on lapsi-taski.

### Lapsi-taskit (Child tasks)
- Task voi luoda lapsi-taskeja, kunhan parentti ei ole luotu parametrilla TaskCreationOptions.DenyChildAttach.
  - Task.Factory.StartNew-metodin kautta luodut taskit sallivat oletuksena lapsi-taskeja.
  - Task.Run-metodin kautta luodut taskit eivät salli lapsi-taskeja!  
- Kun parent taskia odotetaan, parent odottaa myös, että lapsi-task valmistuu.
- *Esim. 512 Child task*.

## Taskien odotus
- Yhtä taskia voidaan odottaa kahdella tapaa:
  - **Wait**-metodilla.
  - **Result**-ominaisuuden kutsulla.
- Montaa taskia voidaan odottaa **Task.WaitAll** tai **Task.WaitAny**-metodeilla.
- **WaitAll** on sama kuin kirjoittaisi seuraavan:
  ```
  // Assume t1, t2 and t3 are tasks:
  var exceptions = new List<Exception>();
  try { t1.Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
  try { t2.Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
  try { t3.Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
  if (exceptions.Count > 0) throw new AggregateException(exceptions);
  ```
- Kirjassa on pieni virhe yllä olevan koodin kohdalla:
  - Kirjan versiossa **Add**-metodin parametriksi annettiin **ex**, vaikka pitäisi olla **ex.InnerException**, kuten esimerkissämme.

## Taskien poikkeuskäsittely
- Taskin **Wait**-metodi heittää **AggregateException**in, jos jokin käsittelemätön poikkeus on lentänyt taskin sisäisessä koodissa.
  - Alkuperäinen poikkeus löytyy **AggregateExceptionin** **InnerException**-ominaisuudesta.
- Jos odotetaan useampaa taskia, saadaan kaikkien taskien poikkeukset kiinni **InnerExceptions**-listan kautta.
- *Esim. 513 Task exception handling*.
- *Esim. 514 Unhandled task exception*.

## Taskien keskeyttäminen (Task cancellation)
- Taskit tukevat suorituksen keskeytystä **CancellationToken**in avulla:
- Tokenin voi antaa **Wait**-metodille, jolloin lentävä poikkeus on **OperationCanceledException**,
  ilman että se on käärittynä **AggregateException**-poikkeuksen sisälle.
 - *Esim. 515 Task cancellation*

## Jatkaminen (Continuation)
- Joskus halutaan aloittaa seuraava tehtävä heti toisen perään.
- Tämä onnistuu **ContinueWith**-metodilla.
- Jos edellinen taski heittää poikkeuksen ja seuraava taski **ContinueWith**-ketjussa ei lue **Exception**-ominaisuutta,
  niin sovellus sulkeutuu käsittelemättömän poikkeuksen vuoksi.
- Taskin jälkeen voidaan aloittaa samanaikaisesti useampi taski.
- *Esim. 516 Task continuation*
- ![alt text](http://www.albahari.com/threading/Continuations.png)
 
## TaskScheduler
- *TaskScheduler* jakaa taskeja säikeille.
- Kaikilla taskeilla on task skeduleri.
- .NET Framework tarjoaa kaksi task scheduleria.
  - *Default scheduler* käyttää thread poolia.
  - *Synchronization context scheduler* on suunniteltu WPF ja Windows Forms -sovelluksille, joissa on vaatimuksena,
    että vain käyttöliittymäsäie päivittää käyttöliittymäkontrolleja.
  ```
  public partial class MyWindow : Window
  {
    TaskScheduler _uiScheduler;   // Declare this as a field so we can use
                                  // it throughout our class.
    public MyWindow()
    {    
      InitializeComponent();
 
      // Get the UI scheduler for the thread that created the form:
      _uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
 
      Task.Factory.StartNew<string> (SomeComplexWebService)
        .ContinueWith (ant => lblResult.Content = ant.Result, _uiScheduler);
    }
 
    string SomeComplexWebService() { ... }
  }
  ```
- Oman task skedulerin voi kirjoittaa perimällä **TaskScheduler**-luokan, mutta tähän on harvoin tarvetta.
- Skedulointiin voi vaikuttaa myös **TaskCompletionSource**n avulla.

## TaskFactory
- **TaskFactory**n avulla voidaan luoda:
  - "tavanomaisia" takseja (**StartNew**).
  - Jatkavia taskeja, kun on olemassa monia aikaisempia taskeja (**ContinueWhenAll**, **ContinueWhenAny**).
  - Taskeja, jotka noudattavan asynkronista ohjelmointimallia (**FromAsync**).

### Oman task factoryn luonti
- TaskFactory ei ole abstrakti, joten sen voi instantioida.
  - Hyödyllistä, kun halutaan luoda paljon taskeja tietyillä parametreilla.
  - Parametrit voi asettaa TaskFactoryn **TaskCreationOptions**, **TaskContinuationOptions** ja **TaskScheduler** ominaisuuksiin,
  - jonka jälkeen kaikki taskit, jotka luodaan StartNew-metodilla, käyttävät aikaisemmin asetettuja arvoja.
- *Esim. 517 Customized TaskFactory*

## TaskCompletionSource
- **TaskCompletionSource**n avulla Taskin työnkulkua voidaan hallita manuaalisesti.
- **TaskCompletionSource** toimii hyvin asynkronisten metodien kanssa.
- *Esim. 518 TaskCompletionSource*

# AggregateException-poikkeusten käsittely
- PLINQ, Parallel-luokka ja Taskit automaattisesti marshalloi poikkeukset kutsujalle.
- Tämä on tärkeää, koska muuten toisessa säikeessä ajettavat virheet voisivat jäädä käsittelemättä.
- Poikkeukset wrapataan AggregateException-poikkeuksen sisälle, koska samanaikaisessa suorituksessa poikkeuksia voi tulla useita samanaikaisesti.
- PLINQ ja Parallel-luokka eivät aloita silmukan seuraavaa iteraatiota, jos edellinen iteraatio heittää poikkeuksen.
  ```
  try
  {
    var query = from i in ParallelEnumerable.Range (0, 1000000)
                select 100 / i;
    // Enumerate query
    ...
  }
  catch (AggregateException aex)
  {
    foreach (Exception ex in aex.InnerExceptions)
      Console.WriteLine (ex.Message);
  }
  ```

## Flatten and Handle

### Flatten
- Monesti **AggregateException**in sisällä voi olla lisää **AggregateException**-poikkeuksia.
- Tilanne on hyvin mahdollinen, jos taskilla ja lapsi-taskeja. Tällöin poikkeuksillekin tulee isä-lapsi-hierarkia.
- Hierarkian voi eliminoida kutsumalla **Flatten**-metodia, joka palauttaa uuden **AggregateException**in, jolla on lista kaikista poikkeuksista ilman hierarkiaa.

### Handle
- **Handle**-metodilla voi käsitellä helposti tietyntyyppiset virheet.
- Metodi ottaa parametriksi predikaatin, joka ajetaan kaikille inner exceptioneille.
- Predikaatti palauttaa true, jos poikkeus on käsitelty.
  ```
  var parent = Task.Factory.StartNew (() => 
  {
    // We’ll throw 3 exceptions at once using 3 child tasks:
   
    int[] numbers = { 0 };
   
    var childFactory = new TaskFactory
     (TaskCreationOptions.AttachedToParent, TaskContinuationOptions.None);
   
    childFactory.StartNew (() => 5 / numbers[0]);   // Division by zero
    childFactory.StartNew (() => numbers [1]);      // Index out of range
    childFactory.StartNew (() => { throw null; });  // Null reference
  });
   
  try { parent.Wait(); }
  catch (AggregateException aex)
  {
    aex.Flatten().Handle (ex =>   // Note that we still need to call Flatten
    {
    if (ex is DivideByZeroException)
    {
      Console.WriteLine ("Divide by zero");
      return true;                           // This exception is "handled"
    }
    if (ex is IndexOutOfRangeException)
    {
      Console.WriteLine ("Index out of range");
      return true;                           // This exception is "handled"   
    }
    return false;    // All other exceptions will get rethrown
    });
  }
  ```
- *Esim. 519 Flatten and Handle**



## Concurrent Collections
- .NET Framework 4 mukana tuli uusia kokoelmaluokkia, joita voidaan käyttää säieturvallisesti ilman lukotusta.
  ![alt text](ConcurrentCollections.png)
- Kokoelmaluokat on viritetty toimimaan suuren rinnakkaisuuden kanssa.
- Pienellä rinnakkaisuudella vanhat kokoelmaluokat ovat suorituskykyisempiä.
- *Esim. 520 ConcurrentDictionary<T> write performance*
- *Esim. 521 ConcurrentDictionary<T> read performance*
- Vaikka yksittäiset toiminnot ovat säieturvallisia, niin useamman metodin kutsuminen peräkkäin ei ohjelman logiikan kannalta tietenkään välttämättä ole oikein.
- Jos toinen säie luettelee concurrent-luokan arvoja, samalla kuin toinen säie muokkaa kokoelmaa, lukija näkee sekoituksen uutta ja vanhaa dataa. Poikkeusta ei tilanteesta lennä.
- **ConcurrentStack**, **ConcurrentQueue** ja **ConcurrentBag** toimivat sisäisesti linkitetyillä listoille. Tämä syö enemmän muistia, mutta toimii vähemmillä lukoilla.
  - Kokoelmaan lisääminen on nopeaa. Kokoelma ei joudu kasvattamaan kokoaan niin kuin **List&lt;T&gt;** voi joutua.
- Concurrent-kokoelmilla on uusia "kokeile-ja-toimi" metodeita, esim. **TryAdd**, **TryTake**, **TryPop**, joilla voidaan suorittaa toimintoja atomisesti.

## SpinLock and SpinWait
- Rinnakkaisohjelmoinnissa lyhyt spinnaus on usein tehokkaampi kuin blockaus, koska se estää kontekstinvaihdon ja kernelin tilasiirtymän.
- *SpinLock* ja *SpinWait* structit auttavat tällaisissa tilanteissa.
  - Ovat scructeja suorituskyvyn vuoksi.
  - Jos annetaan parametrina ilman ref-määrettä, struct kopioituu.

### SpinLock
- Spinnaa säiettä (turhaan!), jotta estetään kontekstinvaihto.
- Sopii tilanteisiin, joissa lukon kesto on erittäin lyhyt.
  - SpinLock aiheuttaa lopulta kontekstin vaihdon (luovuttaa aikaikkunansa), aivan kuten lukko, jos yritetään spinnata liian kauan.
- SpinLock toimii muuten samalla tavalla kuin normaali lukko, paitsi
  - Ovat scructeja
  - Eivät ole reentrant: ei voi kutsua kaksi kertaa Enter-metodia samalle SpinLockille samassa säikeessä.
    - Jos kutsuu, niin lentää joko poikkeus (jos omistava säie on tiedossa) tai tulee deadlock (jos omistava säie ei ole tiedossa).
    - Omistajan seuravauksen voi enabloida, kun luo SpinLockin. Käyttö huonontaa suorityskykyä.
  - IsHeld-metodilla voi tarkistaa onko lukko varattu.
  - IsHeldByCurrentThread-metodilla voi tarkistaa onko kutsuva säie omistaja.
  - SpinLockille ei ole samanlaista helppoa syntaksia kuin on lukolle (lock).
- SpinLockin oikeaoppinen käyttö tapahtuu seuraavasti:
  ```
  var spinLock = new SpinLock (true);   // Enable owner tracking
  bool lockTaken = false;
  try
  {
    spinLock.Enter (ref lockTaken);
    // Do stuff...
  }
  finally
  {
    if (lockTaken) spinLock.Exit();
  }
  ```
- TryEnter-metodilla voidaan antaa timeout.
- *Esim 522 SpinLock**

### SpinWait
- **SpinWait**in avulla voidaan kirjoittaa lukkovapaata koodia, joka spinnaa lukottamisen sijaan.
- Sopii tilanteisiin, joissa lukon kesto on erittäin lyhyt.
- Suorituskykyisempi kuin SpinLock, koska ei lukota.
- Ideana on yrittää tehdä jotain ilman lukkoa ja tarkistaa myöhemmin onnistuiko yritys. Jos ei onnistunut niin yritetään uudestaan.
- Hardcore ratkaisu! Käytetään kun mikään muu ei riitä.
- **SpinWait**iä voi käytätä **SpinUntil** tai **SpinOnce**-metodin avulla:
  ```
  bool _proceed;
  void Test()
  {
    SpinWait.SpinUntil (() => { Thread.MemoryBarrier(); return _proceed; });
    ...
  }
  ```
  Tai:
  ```
  bool _proceed;
  void Test()
  {
    var spinWait = new SpinWait();
    while (!_proceed) { Thread.MemoryBarrier(); spinWait.SpinOnce(); }
    ...
  }

  ```


﻿using System;
using System.Diagnostics;

namespace Threading
{
    public abstract class Example
    {
        public int Code { get; protected set; }
        public string Title { get; protected set; }

        public abstract void Execute();

        protected void TimeAction(Action action)
        {
            var sw = new Stopwatch();
            sw.Start();

            action();

            sw.Stop();
            
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Elapsed: {sw.Elapsed}");
            Console.ResetColor();
        }

        protected TResult TimeAction<TResult>(Func<TResult> action)
        {
            var sw = new Stopwatch();
            sw.Start();

            TResult result = action();

            sw.Stop();
            
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Elapsed: {sw.Elapsed}");
            Console.ResetColor();

            return result;
        }
    }
}
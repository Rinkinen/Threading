﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Threading
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Program
    {
        private static List<Example> Examples { get; set; }

        private static void Main()
        {
            Examples = new List<Example>();
            PopulateExamples();

            var watch = new Stopwatch();

            while (true)
            {
                ListExamples();
                var threadPoolReady = WarmThreadPool();

                Console.Write("Example: ");

                var userInput = Console.ReadLine();

                threadPoolReady.WaitOne();

                if (!int.TryParse(userInput, out var code))
                {
                    Console.WriteLine("Input is not a number.");
                    continue;
                }

                if (code == 0)
                {
                    break;
                }

                var example = GetExample(code);
                Console.WriteLine();

                if (example == null)
                {
                    Console.WriteLine("Example not found with given input.");
                    continue;
                }

                watch.Restart();
                example.Execute();
                watch.Stop();

                Thread.Sleep(500);

                Console.WriteLine();
                Console.WriteLine($"Execution took {watch.Elapsed}.");
                Console.WriteLine("Press any key to see menu again...");
                Console.ReadKey();
            }

            Console.WriteLine("Exiting...");
        }

        /// <summary>
        /// Run few tasks just to warm up thread-pool. This takes away the overhead when creating tasks for the first time
        /// and makes different results more comparable.
        /// </summary>
        private static EventWaitHandle WarmThreadPool()
        {            
            var waitHandle = new ManualResetEvent(false);

            var tasks = Enumerable.Range(1, 16).Select(i => Task.Factory.StartNew(() =>
            {
                var now = DateTime.Now.AddMilliseconds(500);

                // ReSharper disable once EmptyEmbeddedStatement
                while (now > DateTime.Now) ;
            })).ToArray();

            Task.Factory.ContinueWhenAll(tasks, ts =>
            {
                waitHandle.Set();
            });

            return waitHandle;
        }

        private static void PopulateExamples()
        {
            var types = Assembly.GetExecutingAssembly().GetExportedTypes()
                .Where(t => t.IsSubclassOf(typeof(Example)));

            foreach (var type in types)
            {
                Examples.Add((Example)Activator.CreateInstance(type));
            }

            Examples = Examples.OrderBy(ex => ex.Code).ToList();
        }

        private static Example GetExample(int code)
        {
            return Examples.FirstOrDefault(ex => ex.Code.ToString().Equals(code.ToString(), StringComparison.CurrentCultureIgnoreCase));
        }

        private static void ListExamples()
        {
            Console.WriteLine("Available examples: ");
            Console.WriteLine();
            int? previousCode = null;

            foreach (var example in Examples)
            {
                if (previousCode.HasValue && previousCode < example.Code - 1)
                {
                    Console.WriteLine();
                }

                Console.WriteLine($"{example.Code} - {example.Title}");
                previousCode = example.Code;
            }
        }
    }
}
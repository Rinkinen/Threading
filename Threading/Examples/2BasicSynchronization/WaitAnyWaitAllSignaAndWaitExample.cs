﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class WaitAnyWaitAllSignaAndWaitExample : Example
    {
        private static readonly ManualResetEvent WorkerIsReady = new ManualResetEvent(false);
        private static readonly ManualResetEvent MainIsReady = new ManualResetEvent(false);
        private static readonly ManualResetEvent WorkerFinished = new ManualResetEvent(false);

        public WaitAnyWaitAllSignaAndWaitExample()
        {
            Code = 209;
            Title = "WaitAny, WaitAll, SignalAndWait";
        }

        public override void Execute()
        {
            Main();
        }

        private void Main()
        {
            ManualResetEvent[] waitHandles = Enumerable.Range(0, 5).Select(x => new ManualResetEvent(false)).ToArray();

            Task.Run(() => Worker(waitHandles));

            WorkerIsReady.WaitOne();
            Console.WriteLine("Main is ready.");
            MainIsReady.Set();

            foreach (var waitHandle in waitHandles)
            {
                Console.WriteLine("Signaling...");
                waitHandle.Set();
                Thread.Sleep(100);
            }

            Console.WriteLine("Waiting for worker to finish.");
            WorkerFinished.WaitOne();
            Console.WriteLine("Worker is finished.");
        }

        private void Worker(WaitHandle[] waitHandles)
        {
            Console.WriteLine("Worker is ready, waiting for main to be ready.");
            WaitHandle.SignalAndWait(WorkerIsReady, MainIsReady);

            Console.WriteLine("Waiting for the first signal.");
            WaitHandle.WaitAny(waitHandles);
            Console.WriteLine("One wait handle was signaled.");

            WaitHandle.WaitAll(waitHandles);
            Console.WriteLine("All wait handles were signaled.");

            WorkerFinished.Set();
        }
    }
}
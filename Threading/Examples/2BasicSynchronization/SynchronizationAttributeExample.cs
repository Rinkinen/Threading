﻿using System;
using System.Diagnostics;
using System.Runtime.Remoting.Contexts;
using System.Threading;

namespace Threading.Examples
{
    public class SynchronizationAttributeExample : Example
    {
        private static readonly CountdownEvent Countdown = new CountdownEvent(3);

        public SynchronizationAttributeExample()
        {
            Code = 210;
            Title = "Synchronization attribute";
        }

        public override void Execute()
        {
            Countdown.Reset();

            // safeInstance is actually a proxy.
            AutoLock safeInstance = new AutoLock();
            Debugger.Break();

            new Thread(safeInstance.Demo).Start();     // Call the Demo
            new Thread(safeInstance.Demo).Start();     // method 3 times
            safeInstance.Demo();                        // concurrently.

            Countdown.Wait();
        }

        [Synchronization]
        private class AutoLock : ContextBoundObject
        {
            public void Demo()
            {
                Console.Write("Start...");
                Thread.Sleep(1000);
                Console.WriteLine("end");

                Countdown.Signal();
            }
        }
    }
}
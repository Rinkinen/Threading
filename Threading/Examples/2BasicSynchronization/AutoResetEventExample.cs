﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class AutoResetEventExample : Example
    {
        private static readonly EventWaitHandle WaitHandle = new AutoResetEvent(false);

        public AutoResetEventExample()
        {
            Code = 204;
            Title = "AutoResetEvent";
        }

        public override void Execute()
        {
            new Thread(Waiter).Start();

            Thread.Sleep(1000);                  // Pause for a second...
            WaitHandle.Set();                    // Wake up the Waiter.
            Thread.Sleep(1000);                  // Pause for a second...
            WaitHandle.Set();                    // Wake up the Waiter.
        }

        private static void Waiter()
        {
            Console.WriteLine("Waiting...");
            WaitHandle.WaitOne();                // Wait for notification
            Console.WriteLine("Notified #1");
            WaitHandle.WaitOne();                // Wait for notification
            Console.WriteLine("Notified #2");
        }
    }
}

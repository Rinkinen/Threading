﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class MutexInMultipleApplications : Example
    {
        public MutexInMultipleApplications()
        {
            Code = 202;
            Title = "Mutex and multiple applications";
        }

        public override void Execute()
        {
            var mutex = new Mutex(false, "Lukko");

            while (true)
            {
                mutex.WaitOne();

                Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId} is executing: {DateTime.Now.Second}");
                Thread.Sleep(1000);

                mutex.ReleaseMutex();
            }
        }

        // Also check out CloudMutex: https://blogs.msdn.microsoft.com/seendabean/2012/07/02/cloudmutex-synchronize-threads-across-machines-using-azure/
    }
}
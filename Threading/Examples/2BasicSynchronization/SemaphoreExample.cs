﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class SemaphoreExample : Example
    {
        private static readonly int Capacity = 2;
        private static readonly SemaphoreSlim Sem = new SemaphoreSlim(Capacity);

        public SemaphoreExample()
        {
            Code = 203;
            Title = "Semaphrore (nightclub)";
        }

        public override void Execute()
        {
            var tasks = Enumerable.Range(1, 4).Select(i => Task.Run(() => Enter(i)));
            Task.WaitAll(tasks.ToArray());
        }

        private static void Enter(int id)
        {
            // Person entering.
            Console.WriteLine($"Person {id} wants to enter");
            Sem.WaitAsync().Wait();
            Console.WriteLine($"Person {id} is in! {Capacity - Sem.CurrentCount} people in nightclub."); // Only three threads allowed at the same time.

            // Person inside nightclub.
            Thread.Sleep(5000 * id);

            // Person leaving.
            Sem.Release();
            Console.WriteLine($"Person {id} left! {Capacity - Sem.CurrentCount} people in nightclub.");
        }
    }
}
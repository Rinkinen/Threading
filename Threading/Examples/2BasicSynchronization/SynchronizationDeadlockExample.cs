﻿using System;
using System.Threading;

namespace Threading.Examples
{
    //[Synchronization]
    // Try to inherit Example from ContextBoundObject.
    public class SynchronizationDeadlockExample : Example
    {
        public SynchronizationDeadlockExample()
        {
            Code = 212;
            Title = "Synchronization deadlock";
        }

        public override void Execute()
        {
            Deadlock dead1 = new Deadlock();
            Deadlock dead2 = new Deadlock();
            dead1.Other = dead2;
            dead2.Other = dead1;
            new Thread(dead1.Increment).Start();
            dead2.Increment();
        }

        //[Synchronization]
        //[Synchronization(true)]
        private class Deadlock : ContextBoundObject
        {
            private static long _counter;

            public Deadlock Other;

            public Deadlock()
            {
                _counter = 0;
            }

            public void Increment()
            {
                Console.WriteLine("Increment");

                for (int i = 0; i < 100000000; i++)
                {
                    _counter++;
                }

                Console.WriteLine("Calling hello");
                Other.Hello();
            }

            private void Hello()
            {
                Console.WriteLine($"hello {_counter}");
            }
        }
    }
}
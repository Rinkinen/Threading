﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class DeadlockExample : Example
    {
        public DeadlockExample()
        {
            Code = 201;
            Title = "Deadlock";
        }

        public override void Execute()
        {
            var locker1 = new object();
            var locker2 = new object();

            new Thread(() =>
            {
                lock (locker1)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} has locker1 and is waiting for locker2");
                    lock (locker2) // Deadlock
                    {
                        Console.WriteLine("We never come here!");
                    };
                }
            }).Start();

            lock (locker2)
            {
                Thread.Sleep(1000);
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} has locker2 and is waiting for locker1");
                lock (locker1) // Deadlock
                {
                    Console.WriteLine("We never come here!");
                }
            }
        }
    }
}
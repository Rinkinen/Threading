﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Threading;

namespace Threading.Examples
{
    public class SynchronizationBlockExample : Example
    {
        private static readonly CountdownEvent Countdown = new CountdownEvent(3);

        public SynchronizationBlockExample()
        {
            Code = 211;
            Title = "Synchronization block";
        }

        public override void Execute()
        {
            new AutoLock().Test();

            Countdown.Wait();
        }

        [Synchronization]
        private class AutoLock : ContextBoundObject
        {
            private void Demo()
            {
                Console.Write("Start...");
                Thread.Sleep(1000);
                Console.WriteLine("end");

                Countdown.Signal();
            }

            public void Test()
            {
                new Thread(Demo).Start();
                new Thread(Demo).Start();
                new Thread(Demo).Start();
                Console.WriteLine("Reading line from console:");
                Console.ReadLine();
            }
        }
    }
}
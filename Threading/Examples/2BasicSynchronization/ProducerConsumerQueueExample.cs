﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Threading.Examples
{
    public class ProducerConsumerQueueExample : Example
    {
        public ProducerConsumerQueueExample()
        {
            Code = 205;
            Title = "ProducerConsumerQueue";
        }

        public override void Execute()
        {
            Console.WriteLine("Adding tasks to queue...");

            using (var queue = new ProducerConsumerQueue())
            {
                queue.EnqueueTask("Hello");

                for (int i = 0; i < 10; i++)
                {
                    queue.EnqueueTask("Say " + i);
                }

                queue.EnqueueTask("Goodbye!");

                Console.WriteLine("All tasks are queued!");
            }

            // Exiting the using statement calls q's Dispose method, which
            // enqueues a null task and waits until the consumer finishes.
        }

        private class ProducerConsumerQueue : IDisposable
        {
            private readonly EventWaitHandle _wh = new System.Threading.AutoResetEvent(false);
            private readonly Thread _worker;
            private readonly object _locker = new object();
            private readonly Queue<string> _tasks = new Queue<string>();

            public ProducerConsumerQueue()
            {
                _worker = new Thread(Work);
                _worker.Start();
            }

            public void EnqueueTask(string task)
            {
                lock (_locker) _tasks.Enqueue(task);
                _wh.Set();
            }

            public void Dispose()
            {
                EnqueueTask(null);     // Signal the consumer to exit.
                _worker.Join();         // Wait for the consumer's thread to finish.
                _wh.Close();            // Release any OS resources.
            }

            private void Work()
            {
                while (true)
                {
                    string task = null;

                    lock (_locker)
                    {
                        if (_tasks.Count > 0)
                        {
                            task = _tasks.Dequeue();
                            if (task == null) return;
                        }
                    }

                    if (task != null)
                    {
                        Console.WriteLine("Performing task: " + task);
                        Thread.Sleep(1000); // simulate work...
                    }
                    else
                    {
                        _wh.WaitOne(); // No more tasks - wait for a signal
                    }
                }
            }
        }
    }
}
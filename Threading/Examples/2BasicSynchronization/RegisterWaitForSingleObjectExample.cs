﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class RegisterWaitForSingleObjectExample : Example
    {
        private static readonly AutoResetEvent Waiter = new AutoResetEvent(false);

        public RegisterWaitForSingleObjectExample()
        {
            Code = 207;
            Title = "RegisterWaitForSingleObject";
        }

        public override void Execute()
        {
            // This does not block.
            RegisteredWaitHandle reg = ThreadPool.RegisterWaitForSingleObject(Waiter, ThisDoesNotBlockThreadUntilCalled, "some Data", -1, false);

            for (var i = 3; i > 0; i--)
            {
                Console.WriteLine($"Signaling worker in {i} seconds...");
                Thread.Sleep(1000);
            }
            
            Waiter.Set();
            Console.WriteLine("Waiting for worker to finish.");
            Waiter.WaitOne();
            reg.Unregister(Waiter);    // Clean up when we’re done.

            Console.WriteLine("Job done, quitting.");
        }

        private static void ThisDoesNotBlockThreadUntilCalled(object data, bool timedOut)
        {
            // Perform task...
            for (var i = 1; i <= 5; i++)
            {
                Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId} doing work with {data}, loop {i}.");
                Thread.Sleep(1000);
            }

            Waiter.Set();
        }
    }
}
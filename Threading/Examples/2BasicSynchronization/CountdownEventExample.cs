﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class CountdownEventExample : Example
    {
        private static readonly CountdownEvent Countdown = new CountdownEvent(3);

        public CountdownEventExample()
        {
            Code = 206;
            Title = "CountdownEvent";
        }

        public override void Execute()
        {
            Countdown.Reset();

            new Thread(SaySomething).Start("1");
            new Thread(SaySomething).Start("2");
            new Thread(SaySomething).Start("3");

            Countdown.Wait();   // Blocks until Signal has been called 3 times
            Console.WriteLine("All threads have finished speaking!");
        }

        private static void SaySomething(object thing)
        {
            Thread.Sleep(1000 * Convert.ToInt32(thing));
            Console.WriteLine($"Thread {thing} has finished.");
            Countdown.Signal();
        }
    }
}
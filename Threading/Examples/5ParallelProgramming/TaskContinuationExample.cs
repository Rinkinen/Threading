﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class TaskContinuationExample : Example
    {
        public TaskContinuationExample()
        {
            Title = "Task continuation";
            Code = 516;
        }

        public override void Execute()
        {
            // We need to use generic type to express our intent to use Result property.
            Task<int> task1 = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(500);
                Console.WriteLine($"Task#{Task.CurrentId} is returning {Task.CurrentId}.");
                return Task.CurrentId ?? 0;
            });

            task1.ContinueWith(previousTask =>
            {
                Thread.Sleep(500);
                Console.WriteLine($"Previous task#{previousTask.Id} returned {previousTask.Result}. Task#{Task.CurrentId} is returning {Task.CurrentId}.");
                return Task.CurrentId ?? 0;
            }).ContinueWith(previousTask =>
            {
                Thread.Sleep(500);
                Console.WriteLine($"Previous task#{previousTask.Id} returned {previousTask.Result}. Last Task#{Task.CurrentId} is returning {Task.CurrentId}.");
                return Task.CurrentId ?? 0;
            }).Wait(); // Remember to wait!

            Console.WriteLine(new string('-', 50));

            // Exception in previous task can be used in the following task.
            Task<int> mainTask = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(500);

                Console.WriteLine($"Task#{Task.CurrentId} is throwing exception.");
                throw new NullReferenceException("The Error!");

                Console.WriteLine($"Main Task#{Task.CurrentId} is returning 456.");
                return 456;
            });

            // Error handling.
            var errorHandler = mainTask.ContinueWith(exceptionalTask =>
            {
                Thread.Sleep(500);
                Console.WriteLine($"Previous task#{exceptionalTask.Id} faulted, exception: {exceptionalTask.Exception.InnerException.Message}. Task#{Task.CurrentId} is returning {Task.CurrentId}.");
            }, TaskContinuationOptions.OnlyOnFaulted);

            // Result data handling.
            var dataHandler = mainTask.ContinueWith(succeededTask =>
            {
                Thread.Sleep(500);
                Console.WriteLine(
                    $"Main task #{succeededTask.Id} returned {succeededTask.Result}. Saving value to database.");
                return succeededTask.Result;
            }, TaskContinuationOptions.NotOnFaulted);

            // This is ugly, don't know how to do it otherwise.
            Task.Factory.ContinueWhenAll(new[] { errorHandler, dataHandler }, tasks =>
            {
                Thread.Sleep(500);
                Console.WriteLine("Finalizer for both paths.");
            }).Wait();
        }
    }
}
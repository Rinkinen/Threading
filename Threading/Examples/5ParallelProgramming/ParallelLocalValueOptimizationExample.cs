﻿using System;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ParallelLocalValueOptimizationExample : Example
    {
        public ParallelLocalValueOptimizationExample()
        {
            Title = "Parallel local value optimization";
            Code = 510;
        }

        public override void Execute()
        {
            int millionLocks = 0;

            TimeAction(() =>
            {
                object locker = new object();
                double total = 0;
                Parallel.For(0, 10000000, i =>
                {
                    // One million locks... BAD!
                    lock (locker)
                    {
                        total += Math.Sqrt(i);
                        millionLocks++;
                    }
                });

                Console.WriteLine($"Million locks: {millionLocks}.");
            });

            Console.WriteLine();

            int feverLocks = 0;

            TimeAction(() =>
            {
                object locker = new object();
                double grandTotal = 0;

                Parallel.For(0, 10000000,

                    () => 0.0,                        // Initialize the local value.

                    (i, state, localTotal) =>         // Body delegate. Notice that it
                        localTotal + Math.Sqrt(i),    // returns the new local total.

                    localTotal =>                                  // Add the local value
                    {
                        lock (locker)
                        {
                            grandTotal += localTotal;
                            feverLocks++;
                        }
                    }    // to the master value.
                );

                Console.WriteLine($"Few locks: {feverLocks}.");
            });
        }
    }
}
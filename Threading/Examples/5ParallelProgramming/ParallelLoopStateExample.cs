﻿using System;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ParallelLoopStateExample : Example
    {
        public ParallelLoopStateExample()
        {
            Title = "ParallelLoopState";
            Code = 509;
        }

        public override void Execute()
        {
            ParallelLoopResult result = Parallel.ForEach("Hello, world", (c, loopState) =>
           {
               if (c == ',')
                   loopState.Break();
               else
                   Console.Write(c);
           });            

            Console.WriteLine();
            Console.WriteLine($"IsCompleted: {result.IsCompleted}, LowestBreakIteration: {result.LowestBreakIteration}");
        }
    }
}
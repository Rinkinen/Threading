﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class TaskCancellationExample : Example
    {
        public TaskCancellationExample()
        {
            Title = "Task cancellation";
            Code = 515;
        }

        public override void Execute()
        {
            IEnumerable<int> million = Enumerable.Range(3, 1000000);

            var cancelSource = new CancellationTokenSource();

            var task = new Task(() =>
            {
                foreach (var number in million)
                {
                    cancelSource.Token.ThrowIfCancellationRequested();

                    if (Enumerable.Range(2, (int)Math.Sqrt(number)).All(i => number % i > 0))
                    {
                        Console.Write($"{number} ");
                    }

                    Thread.Sleep(10);
                }
            }, cancelSource.Token);

            task.Start();
            Console.WriteLine("Press [Enter] to cancel...");
            Console.ReadLine();

            cancelSource.Cancel();

            try
            {
                task.Wait(cancelSource.Token);
                //task.Wait();
            }
            catch (AggregateException agg)
            {
                var ex = (TaskCanceledException)agg.InnerException;
                Console.WriteLine($"{ex.Message}, IsCancellationRequested: {ex.CancellationToken.IsCancellationRequested}, TaskId: {ex.Task.Id}.");
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine($"{ex.Message}, IsCancellationRequested: {ex.CancellationToken.IsCancellationRequested}, TaskId: <not available>");
            }
        }
    }
}
﻿using System;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class CustomizedTaskFactoryExample : Example
    {
        public CustomizedTaskFactoryExample()
        {
            Title = "Customized TaskFactory";
            Code = 517;
        }

        public override void Execute()
        {
            // Uncustomized TaskFactory.
            var uncustomizedTask = Task.Factory.StartNew(() => { });
            Console.WriteLine($"Default Task. CreationOptions: {uncustomizedTask.CreationOptions}, Factory.ContinuationOptions: {Task.Factory.ContinuationOptions}");

            var taskCreationOptions = TaskCreationOptions.LongRunning | TaskCreationOptions.AttachedToParent;
            var taskContinuationOptions = TaskContinuationOptions.LongRunning | TaskContinuationOptions.AttachedToParent;
            // Customized TaskFactory.
            var factory = new TaskFactory(taskCreationOptions, taskContinuationOptions);
            // Every task is now created with custom parameters.
            var customizedTask = factory.StartNew(() => { });
            Console.WriteLine($"Customized Task. CreationOptions: {customizedTask.CreationOptions}, Factory.ContinuationOptions: {factory.ContinuationOptions}");
        }
    }
}
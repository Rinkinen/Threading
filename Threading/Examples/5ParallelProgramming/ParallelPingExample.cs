﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Policy;

namespace Threading.Examples
{
    public class ParallelPingExample : Example
    {
        public ParallelPingExample()
        {
            Title = "Parallel ping";
            Code = 504;
        }

        public override void Execute()
        {
            var sites = new[]
            {
                "www.albahari.com", // Doesn't respond to ping.
                "www.linqpad.net",  // Doesn't respond to ping.
                "www.oreilly.com",
                "www.takeonit.com",
                "stackoverflow.com",
                "www.rebeccarey.com"
            };

            var stopwatch = new Stopwatch();


            var roundtripTimes = sites
                .AsParallel()
                .WithDegreeOfParallelism(sites.Length)
                .Select(site => new Tuple<string, PingReply>(site, new Ping().Send(site)))
                .Select(tuple => new { Site = tuple.Item1, tuple.Item2.Address, tuple.Item2.RoundtripTime });

            Console.WriteLine("PLINQ");
            stopwatch.Start();
            foreach (var time in roundtripTimes)
            {
                Console.WriteLine($"Site:{time.Site} ({time.Address}), round-trip time: {time.RoundtripTime}.");
            }
            stopwatch.Stop();
            Console.WriteLine($"Operation took: {stopwatch.Elapsed}.");

            Console.WriteLine();
            Console.WriteLine("Foreach");
            stopwatch.Restart();
            foreach (var site in sites)
            {
                var reply = new Ping().Send(site);
                Console.WriteLine($"Site:{site} ({reply.Address}), round-trip time: {reply.RoundtripTime}.");
            }
            stopwatch.Stop();
            Console.WriteLine($"Operation took: {stopwatch.Elapsed}.");
        }
    }
}
﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples._5ParallelProgramming
{
    public class SpinLockExample : Example
    {
        private SpinLock _spinLock = new SpinLock(false); // Disable owner tracking.
        private object _locker = new object();

        public SpinLockExample()
        {
            Title = "SpinLock";
            Code = 522;
        }

        public override void Execute()
        {                   
            Console.WriteLine("SpinLock");
            TimeAction(() =>
            {
                var counter = 0;

                Parallel.For(0, 1000000, i =>
                {
                    IncreaseValueWithSpinLock(ref counter, 1);
                });

                Console.WriteLine($"Counter: {counter}.");
            });

            Console.WriteLine("Monitor");
            TimeAction(() =>
            {
                var counter = 0;

                Parallel.For(0, 1000000, i =>
                {
                    IncreaseValueWithMonitor(ref counter, 1);
                });

                Console.WriteLine($"Counter: {counter}.");
            });
        }

        private void IncreaseValueWithSpinLock(ref int counter, int value)
        {
            var lockTaken = false;
            try
            {
                _spinLock.Enter(ref lockTaken);
                counter += value;
            }
            finally
            {
                if (lockTaken) _spinLock.Exit();
            }
        }

        private void IncreaseValueWithMonitor(ref int counter, int value)
        {
            var lockTaken = false;
            try
            {
                Monitor.Enter(_locker, ref lockTaken);
                counter += value;
            }
            finally
            {
                if (lockTaken) Monitor.Exit(_locker);
            }
        }
    }
}
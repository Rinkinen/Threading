﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ConcurrentDictionaryWritePerformanceExample : Example
    {
        public ConcurrentDictionaryWritePerformanceExample()
        {
            Title = "ConcurrentDictionary<T> write performance";
            Code = 520;
        }

        public override void Execute()
        {
            Console.WriteLine("Single-thread test:");
            Console.WriteLine();

            var dictionary = new Dictionary<int, int>();

            TimeAction(() =>
            {
                Console.WriteLine($"Dictionary<int, int> with 10 000 000 adds.");

                for (int i = 0; i < 10000000; i++)
                {
                    dictionary.Add(i, i);
                }
            });

            Console.WriteLine($"Dictionary count: {dictionary.Count}.");
            Console.WriteLine();

            var concurrentDictionary = new ConcurrentDictionary<int, int>();

            TimeAction(() =>
            {
                Console.WriteLine($"ConcurrentDictionary<int, int> with 10 000 000 adds.");

                for (int i = 0; i < 10000000; i++)
                {
                    concurrentDictionary.TryAdd(i, i);
                }
            });

            Console.WriteLine($"Concurrent dictionary count: {concurrentDictionary.Count}.");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Multi-thread test:");
            Console.WriteLine();

            var options = new ParallelOptions { MaxDegreeOfParallelism = 8 };

            dictionary = new Dictionary<int, int>();

            TimeAction(() =>
            {
                Console.WriteLine($"Dictionary<int, int> with 10 000 000 adds.");

                Parallel.For(0, 10000000, options, i =>
                {
                    lock (dictionary)
                    {
                        dictionary.Add(i, i);
                    }
                });
            });

            Console.WriteLine($"Dictionary count: {dictionary.Count}.");
            Console.WriteLine();

            concurrentDictionary = new ConcurrentDictionary<int, int>();

            TimeAction(() =>
            {
                Console.WriteLine($"ConcurrentDictionary<int, int> with 10 000 000 adds.");

                Parallel.For(0, 10000000, options, i =>
                {
                    concurrentDictionary.TryAdd(i, i);
                });
            });

            Console.WriteLine($"Concurrent dictionary count: {concurrentDictionary.Count}.");
        }
    }
}
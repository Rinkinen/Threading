﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class PLinqOptimizationExample : Example
    {
        private readonly List<int> _numbers = new List<int>(ParallelEnumerable.Range(1, 50));

        public PLinqOptimizationExample()
        {
            Title = "PLINQ optimization";
            Code = 506;
        }

        public override void Execute()
        {
            Console.WriteLine("Output optimization");

            // Without output optimization.
            TimeAction(() =>
            {
                Console.Write("No optimization:   ");

                var chars = "abcdefghijklmnopqrstu".AsParallel().Select(char.ToUpper);
                // Results are collated here for nothing as they are just enumerated.
                foreach (var c in chars)
                {
                    Console.Write(c);
                }
            });

            // With output optimization: ForAll. DOES NOT SUPPORT AsOrdered!
            TimeAction(() =>
            {
                Console.Write("With optimization: ");
                "abcdefghijklmnopqrstu".AsParallel().Select(char.ToUpper).ForAll(Console.Write);
            });

            Console.WriteLine();

            TimeAction(() =>
            {
                Console.Write("Range partioning: ");
                // List variable implements IList, so PLINQ chooses range partitioning.
                _numbers.AsParallel().Sum(SleepyTask);
            });

            TimeAction(() =>
            {
                Console.Write("Chunk partioning: ");
                // Here we force chunk partioning.
                Partitioner.Create(_numbers).AsParallel().Sum(SleepyTask);
            });
        }

        private int SleepyTask(int i)
        {
            // Artificial duration for small indexes.
            if (i < 25)
            {
                Thread.Sleep(100);
            }

            return i * i;
        }
    }
}
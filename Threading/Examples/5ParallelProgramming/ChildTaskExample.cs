﻿using System;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ChildTaskExample : Example
    {
        public ChildTaskExample()
        {
            Title = "Child Task";
            Code = 512;
        }

        public override void Execute()
        {
            Task parent = Task.Factory.StartNew(() =>  // Allows children by default.
            //Task parent = Task.Run(() =>             // Does not allow children, because uses TaskCreationOptions.DenyChildAttach.
            //Task parent = new Task(() =>             // Allows children by default.
            {
                Task.Delay(500).Wait();
                Console.WriteLine("I am a parent");

                Task.Factory.StartNew(() =>
                {
                    Task.Delay(2000).Wait();
                    Console.WriteLine("I am detached");
                });

                Task.Factory.StartNew(() =>
                {
                    Task.Delay(1000).Wait();
                    Console.WriteLine("I am a child");
                }, TaskCreationOptions.AttachedToParent);
            });
            
            parent.Wait();
        }
    }
}
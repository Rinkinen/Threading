﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ParallelExample : Example
    {
        public ParallelExample()
        {
            Title = "Parallel class";
            Code = 508;
        }

        public override void Execute()
        {
            var resultParallelFor = TimeAction(() =>
            {
                var roots = new ConcurrentBag<decimal>();

                Console.WriteLine("Parallel.For");
                // Re-uses same tasks, which use threadpool which re-use same threads.
                Parallel.For(0, 1000, i =>
                {
                    CalculateRoots(i, roots);
                });

                return roots;
            }).OrderBy(n => n).ToList();

            Console.WriteLine();

            var resultTaskRun = TimeAction(() =>
            {
                var roots = new ConcurrentBag<decimal>();

                // Creates new task for every loop which use threadpool which re-use same threads.
                Console.WriteLine("Task.Run");
                var tasks = Enumerable.Range(0, 1000).Select(i => new Task(() => { CalculateRoots(i, roots); })).ToList();

                tasks.ForEach(t => t.Start());
                Task.WaitAll(tasks.ToArray());

                return roots;
            }).OrderBy(n => n).ToList();

            Console.WriteLine();
            resultParallelFor.ForEach(root => Console.WriteLine($"Parallel root: {root}"));
            resultTaskRun.ForEach(root => Console.WriteLine($"Parallel root: {root}"));
        }

        private static void CalculateRoots(int i, ConcurrentBag<decimal> roots)
        {
            decimal sqrt = 0;
            for (int j = 0; j < i; j++)
            {
                sqrt = Convert.ToDecimal(Math.Sqrt(j));
            }

            if (i % 100 == 0)
            {
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId}, TaskID: {Task.CurrentId}.");
                roots.Add(sqrt);
            }
        }
    }
}
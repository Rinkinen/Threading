﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Threading
{
    public class ParallelSpellcheckerExample : Example
    {
        private struct IndexedWord
        {
            public string Word;
            public int Index;
        }

        public ParallelSpellcheckerExample()
        {
            Title = "Parallel spellchecker";
            Code = 502;
        }

        public override void Execute()
        {
            // Contains about 150,000 words.
            var wordLookup = new HashSet<string>(File.ReadAllLines("words.txt"), StringComparer.InvariantCultureIgnoreCase);

            var random = new Random(Guid.NewGuid().GetHashCode());
            string[] wordList = wordLookup.ToArray();

            string[] wordsToTest = Enumerable.Range(0, 1000000)
                .Select(i => wordList[random.Next(0, wordList.Length)])
                .ToArray();

            wordsToTest[12345] = "woozsh";     // Introduce a couple
            wordsToTest[23456] = "wubsie";     // of spelling mistakes

            var words = wordsToTest
                .AsParallel()
                .Select((word, index) => new IndexedWord { Word = word, Index = index })
                .Where(iword => !wordLookup.Contains(iword.Word))
                .OrderBy(iword => iword.Index);

            foreach (var word in words)
            {
                Console.WriteLine($"Index: {word.Index}, Word: {word.Word}");
            }
        }
    }
}
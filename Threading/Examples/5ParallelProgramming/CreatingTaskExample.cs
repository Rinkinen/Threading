﻿using System;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class CreatingTaskExample : Example
    {
        public CreatingTaskExample()
        {
            Title = "Creating Task";
            Code = 511;
        }

        public override void Execute()
        {
            // Giving parameters via Factory.StartNew.
            var task = Task.Factory.StartNew(Greet, "Hello");
            task.Wait();  // Wait for task to complete.

            Console.WriteLine();

            // Giving parameters via Task.Run.
            task = new Task(() => Greet("Hello"));
            task.Start();
            task.Wait();

            Console.WriteLine();

            // Task can be named by givin a value for AsyncState.            
            task = Task.Factory.StartNew(state => Greet("Hello"), "Greeting");
            Console.WriteLine(task.AsyncState);
            task.Wait();
        }

        private static void Greet(object state)
        {
            Console.WriteLine(state);
        }
    }
}
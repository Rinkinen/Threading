﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class PLinqCancellationExample : Example
    {
        public PLinqCancellationExample()
        {
            Title = "PLINQ Cancellation";
            Code = 505;
        }

        public override void Execute()
        {
            IEnumerable<int> million = Enumerable.Range(3, 1000000);

            var cancelSource = new CancellationTokenSource();

            var primeNumberQuery = million
                .AsParallel()
                .WithCancellation(cancelSource.Token)
                .Where(n => Enumerable.Range(2, (int)Math.Sqrt(n)).All(i => n % i > 0));

            var cancelThread = new Thread(() =>
            {
                Thread.Sleep(100); // Cancel query after
                cancelSource.Cancel(); // 100 milliseconds.
            });
            cancelThread.Start();
            
            try
            {
                foreach (var primeNumber in primeNumberQuery)
                {
                    Console.Write($"{primeNumber} ");
                }                
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Query canceled in another thread.");
            }
        }
    }
}
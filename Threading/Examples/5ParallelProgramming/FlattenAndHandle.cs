﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class FlattenAndHandleExample : Example
    {
        public FlattenAndHandleExample()
        {
            Title = "Flatten and Handle";
            Code = 519;
        }

        public override void Execute()
        {
            var parent = Task.Factory.StartNew(() =>
           {
               // We’ll throw 3 exceptions at once using 3 child tasks:

               int[] numbers = { 0 };

               var childFactory = new TaskFactory(TaskCreationOptions.AttachedToParent, TaskContinuationOptions.None);

               childFactory.StartNew(() => 5 / numbers[0]);   // Division by zero
               childFactory.StartNew(() => numbers[1]);      // Index out of range
               childFactory.StartNew(() => throw null);  // Null reference
           });

            try
            {
                parent.Wait();
            }
            catch (AggregateException aex)
            {
                Debugger.Break();

                var flattened = aex.Flatten();
                flattened.Handle(ex =>   // Note that we still need to call Flatten
                {
                    if (ex is DivideByZeroException)
                    {
                        Console.WriteLine("Divide by zero");
                        return true;                           // This exception is "handled"
                    }
                    if (ex is IndexOutOfRangeException)
                    {
                        Console.WriteLine("Index out of range");
                        return true;                           // This exception is "handled"
                    }
                    // Try to comment out one of the exception handler.
                    if (ex is NullReferenceException)
                    {
                        Console.WriteLine("Null exception");
                        return true;
                    }

                    return false;    // All other exceptions will get rethrown
                });
            }
        }
    }
}
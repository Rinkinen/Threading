﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ConcurrentDictionaryReadPerformanceExample : Example
    {
        public ConcurrentDictionaryReadPerformanceExample()
        {
            Title = "ConcurrentDictionary<T> read performance";
            Code = 521;
        }

        public override void Execute()
        {
            var dictionary = new Dictionary<int, int>();
            var concurrentDictionary = new ConcurrentDictionary<int, int>();

            for (int i = 0; i < 10000000; i++)
            {
                dictionary.Add(i, i);
                concurrentDictionary.TryAdd(i, i);
            }

            Console.WriteLine($"Dictionary count: {dictionary.Count}.");
            Console.WriteLine($"Concurrent dictionary count: {concurrentDictionary.Count}.");
            Console.WriteLine();
            Console.WriteLine("Single-thread test:");
            Console.WriteLine();

            TimeAction(() =>
            {
                Console.WriteLine($"Dictionary<int, int> with 10 000 000 reads.");

                for (int i = 0; i < 10000000; i++)
                {
                    var value = dictionary[i];
                }
            });

            Console.WriteLine();

            TimeAction(() =>
            {
                Console.WriteLine($"ConcurrentDictionary<int, int> with 10 000 000 reads.");

                for (int i = 0; i < 10000000; i++)
                {
                    var value = concurrentDictionary[i];
                }
            });

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Multi-thread test:");
            Console.WriteLine();

            var options = new ParallelOptions { MaxDegreeOfParallelism = 8 };

            TimeAction(() =>
            {
                Console.WriteLine($"Dictionary<int, int> with 10 000 000 reads.");

                Parallel.For(0, 10000000, options, i =>
                {
                    lock (dictionary)
                    {
                        var value = dictionary[i];
                    }
                });
            });

            Console.WriteLine();

            TimeAction(() =>
            {
                Console.WriteLine($"ConcurrentDictionary<int, int> with 10 000 000 reads.");

                Parallel.For(0, 10000000, options, i =>
                {
                    var value = concurrentDictionary[i];
                });
            });
        }
    }
}
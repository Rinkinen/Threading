﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples._5ParallelProgramming
{
    public class SpinWaitExample : Example
    {
        public SpinWaitExample()
        {
            Title = "SpinWait";
            Code = 523;
        }

        public override void Execute()
        {
            var counter = 0;

            Parallel.For(0, 2, i => { Increase(ref counter, 1); });

            Console.WriteLine($"Counter: {counter}");
        }

        private void Increase(ref int counter, int value)
        {
            var spinWait = new SpinWait();
            while (true)
            {
                int snapshot1 = counter;
                Thread.MemoryBarrier();
                int sum = snapshot1 + value;
                // CompareExchange returns the value of counter. If it differs from snapshot1,
                // some other thread updated the counter and this thread has to try again.
                int snapshot2 = Interlocked.CompareExchange(ref counter, sum, snapshot1);
                if (snapshot1 == snapshot2) return;   // No one preempted us.
                spinWait.SpinOnce();
            }
        }
    }
}
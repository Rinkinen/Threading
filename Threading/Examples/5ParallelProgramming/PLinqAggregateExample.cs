﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class PLinqAggregateExample : Example
    {
        public PLinqAggregateExample()
        {
            Title = "PLINQ aggregate";
            Code = 507;
        }

        public override void Execute()
        {
            IEnumerable<int> numbers = new[] { 2, 3, 4 };
            //IEnumerable<int> numbers = new[] { 0, 2, 3, 4 };

            // LINQ:  f(f(f(0, 2),3),4)
            // PLINQ: f(f(0,2),f(3,4))
            // PLINQ: f(f(0,3),f(2,3))
            // PLINQ: f(f(0,2),f(f(0,3),f(0,4)))

            int withoutSeed = numbers.Aggregate((total, n) => Aggregate(total, n)); //   2 + 3*3 + 4*4 = 27, wrong
            int withSeed = numbers.Aggregate(0, (total, n) => Aggregate(total, n)); // 2*2 + 3*3 + 4*4 = 29, right

            Console.WriteLine($"Without seed: {withoutSeed}");
            Console.WriteLine($"With seed: {withSeed}");
            Console.WriteLine();

            numbers = Enumerable.Range(2, 10).ToList();

            TimeAction(() =>
            {
                int aggregate = numbers.Aggregate(0, (total, n) => Aggregate(total, n));
                Console.WriteLine($"Sequential: {aggregate}");
            });

            Console.WriteLine();

            TimeAction(() =>
            {
                // Adding zero to the data does not help in PLINQ, if the accumulator is not communitative and associative.
                int aggregate = numbers.AsParallel().Aggregate(0, (total, n) => Aggregate(total, n));
                Console.WriteLine($"Parallel (but still sequential): {aggregate}");
            });

            Console.WriteLine();

            TimeAction(() =>
            {
                int aggregate = numbers.AsParallel().WithDegreeOfParallelism(2).Aggregate(
                    // Seed factory.
                    () => 0,
                    // Update accumulator function.
                    (localTotal, n) => Aggregate(localTotal, n),
                    // Combine accumulator function.
                    (mainTotal, localRotal) => mainTotal + localRotal,
                    // Result selector.
                    finalResult => finalResult);

                Console.WriteLine($"Parallel: {aggregate}");
            });
        }

        private static int Aggregate(int total, int n)
        {
            Thread.Sleep(1000);

            return total + n * n;
        }
    }
}
﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class TaskCompletionSourceExample : Example
    {
        public TaskCompletionSourceExample()
        {
            Title = "TaskCompletionSource";
            Code = 518;
        }

        public override void Execute()
        {
            try
            {
                var task = GetWebPage("http://www.google.com");
                var webPage = task.Result;

                Console.WriteLine(webPage);
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        private Task<string> GetWebPage(string url)
        {
            var completionSource = new TaskCompletionSource<string>();
            var webClient = new WebClient();
            webClient.DownloadStringCompleted += WebClient_DownloadStringCompleted;
            webClient.DownloadStringAsync(new Uri(url), completionSource);
            //webClient.CancelAsync();

            return completionSource.Task;
        }

        private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var completionSource = (TaskCompletionSource<string>)e.UserState;

            if (e.Cancelled)
            {
                completionSource.SetCanceled();
                return;
            }

            completionSource.SetResult(e.Result.Substring(0, 50));
        }
    }
}
﻿using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class UnhandledTaskExceptionExample : Example
    {
        public UnhandledTaskExceptionExample()
        {
            Title = "Unhandled Task exception";
            Code = 514;
        }

        public override void Execute()
        {
            new Task(BadDivision).Start();
            //new Thread(BadDivision).Start();
        }

        private static void BadDivision()
        {
            var seven = 7;
            var zero = 0;

            var division = seven / zero;
        }
    }
}
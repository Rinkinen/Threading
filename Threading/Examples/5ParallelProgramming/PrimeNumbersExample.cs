﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Threading.Examples
{
    public class PrimeNumbersExample : Example
    {
        public PrimeNumbersExample()
        {
            Title = "Prime numbers with PLINQ";
            Code = 501;
        }

        public override void Execute()
        {
            const int tenMillion = 10000000;

            List<int> list = new List<int> { 1, 2, 3 };
            list.AsParallel().Where(x => x < 2).Take(2).ToList();

            // Calculate prime numbers using a simple (unoptimized) algorithm.
            IEnumerable<int> numbers = Enumerable.Range(1, tenMillion);

            var parallelQuery = numbers.AsParallel()
                // Don't need to call AsParallel multiple times.
                .Where(n => Enumerable.Range(2, (int)Math.Sqrt(n)).All(i => n % i > 0));

            int count = 0;

            // Parallel Execution Ballistics:
            // - A sequential query is powered entirely by the consumer in a "pull" fashion:
            //   each element from the input sequence is fetched exactly when required by the consumer.
            // - A parallel query ordinarily uses independent threads to fetch elements from the
            //   input sequence slightly ahead of when they’re needed by the consumer.
            foreach (var primeNumber in parallelQuery)
            {
                if (++count % 100000 == 0)
                {
                    Console.WriteLine(primeNumber);
                }
            }
        }
    }
}
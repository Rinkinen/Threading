﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class TaskExceptionHandlingExample : Example
    {
        public TaskExceptionHandlingExample()
        {
            Title = "Task exception handling";
            Code = 513;
        }

        public override void Execute()
        {
            var tasks = Enumerable.Range(1, 5).Select(i => new Task(ExecuteMethodWithAritmethicException)).ToList();
            tasks.ForEach(t => t.Start());

            try
            {
                Console.WriteLine("WaitAll - Groups all exceptions inside single AggregateException.");
                Task.WaitAll(tasks.ToArray());
            }
            catch (AggregateException ex)
            {
                PrintExceptions(ex);
            }

            Console.WriteLine();
            Console.WriteLine("-----");
            Console.WriteLine("Manually group exceptions inside single AggregateException.");

            // The following is same as calling Task.WaitAll(tasks.ToArray());
            // Assume t1, t2 and t3 are tasks:
            var exceptions = new List<Exception>();
            try { tasks[0].Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
            try { tasks[1].Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
            try { tasks[2].Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
            try { tasks[3].Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }
            try { tasks[4].Wait(); } catch (AggregateException ex) { exceptions.Add(ex.InnerException); }

            try
            {
                throw new AggregateException(exceptions);
            }
            catch (AggregateException ex)
            {
                PrintExceptions(ex);
            }
        }

        private static void PrintExceptions(AggregateException ex)
        {
            Console.WriteLine($"Exception: {ex.GetType().Name}, {ex.Message}");
            Console.WriteLine($"Exception.InnerException: {ex.InnerException.GetType().Name}, {ex.InnerException.Message}");
            Console.WriteLine();

            foreach (var exception in ex.InnerExceptions)
            {
                Console.WriteLine($"Exception.InnerExceptions: {exception.GetType().Name}, {exception.Message}");
            }
        }

        private static void ExecuteMethodWithAritmethicException()
        {
            try
            {
                int seven = 7;
                int zero = 0;

                var division = seven / zero;
            }
            catch (ArithmeticException ex)
            {
                throw new ArithmeticException($"{ex.Message} TaskId: {Task.CurrentId}");
            }
        }
    }
}
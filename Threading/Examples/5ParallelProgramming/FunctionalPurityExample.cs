﻿using System;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class FunctionalPurityExample : Example
    {
        public FunctionalPurityExample()
        {
            Title = "Functional purity";
            Code = 503;
        }

        public override void Execute()
        {
            // The following query multiplies each element by its position.
            // Given an input of Enumerable.Range(0,999), it should output squares.

            int i = 0;
            var query = Enumerable.Range(0, 999).AsParallel().Select(n => n * i++);
            //var query = Enumerable.Range(0, 999).AsParallel().AsOrdered().Select(n => n * Interlocked.Increment(ref i));
            //var query = Enumerable.Range(0, 999).AsParallel().Select((n, index) => n * index);

            Console.WriteLine(String.Join(",", query));
        }
    }
}
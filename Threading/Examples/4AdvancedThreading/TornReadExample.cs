﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class TornReadExample : Example
    {
        private const int Iterations = 20000;
        private const decimal Denominator = 200;
        private const decimal MaxPossibleValue = Iterations / Denominator; // 100.

        /// <summary>
        /// Decimal is 128-bit data type.
        /// </summary>
        private static decimal SharedState { get; set; }

        public TornReadExample()
        {
            Title = "128-bit torn read";
            Code = 403;
        }

        public override void Execute()
        {
            var writerThread = new Thread(WriterThreadEntry);
            var readerThread = new Thread(ReaderThreadEntry);

            writerThread.Start();
            readerThread.Start();

            writerThread.Join();
            readerThread.Join();
        }

        private static void WriterThreadEntry()
        {
            for (int i = 0; i < Iterations; ++i)
            {
                SharedState = i / Denominator;
            }
        }

        private static void ReaderThreadEntry()
        {
            for (int i = 0; i < Iterations; ++i)
            {
                var sharedStateLocal = SharedState;
                if (sharedStateLocal > MaxPossibleValue)
                {
                    Console.WriteLine("Impossible value detected: " + sharedStateLocal);
                }
            }
        }
    }
}
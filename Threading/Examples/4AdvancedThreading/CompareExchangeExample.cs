﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class CompareExchangeExample : Example
    {
        private static readonly Barrier Barrier = new Barrier(10);

        public CompareExchangeExample()
        {
            Title = "CompareExchange";
            Code = 405;
        }

        public override void Execute()
        {
            object o = null;

            var tasks = Enumerable.Range(1, 10).Select(i => Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Waiting the all threads to start.");
                    Barrier.SignalAndWait();

                    Interlocked.CompareExchange(ref o, new object(), null);
                    //Interlocked.CompareExchange(ref o, InitializeObject(), null);

                    Console.WriteLine($"Object hash: {o.GetHashCode()}");
                })).ToList();
            
            Task.WaitAll(tasks.ToArray());
        }

        private object InitializeObject()
        {
            Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} is creating the object");
            return new object();
        }
    }
}
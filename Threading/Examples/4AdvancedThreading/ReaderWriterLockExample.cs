﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class ReaderWriterLockExample : Example
    {
        private static ReaderWriterLockSlim _rwLock;
        private static List<int> _items;
        private static Random _random;

        public ReaderWriterLockExample()
        {
            Title = "ReaderWriterLockSlim";
            Code = 410;
        }

        public override void Execute()
        {
            _rwLock = new ReaderWriterLockSlim();
            _items = new List<int>();
            _random = new Random();

            var threads = new List<Thread>
            {
                new Thread(Read),
                new Thread(Read),
                new Thread(Read),
                new Thread(Write),
                new Thread(Write)
            };

            threads.Take(3).ToList().ForEach(t => t.Start());  // Start readers.
            threads.Skip(3).First().Start("A"); // Start writer A.
            threads.Skip(4).First().Start("B"); // Start writer B.

            Console.WriteLine("Press [Enter] to abort threads.");
            Console.ReadLine();

            threads.ForEach(t => t.Abort());
        }

        private static void Read()
        {
            try
            {
                while (true)
                {
                    _rwLock.EnterReadLock();
                    //_rwLock.EnterReadLock();
                    foreach (int i in _items)
                    {
                        Thread.Sleep(10);
                    }

                    _rwLock.ExitReadLock();
                }
            }
            catch (ThreadAbortException)
            {
                Console.WriteLine("Thread aborted");
            }
        }

        private static void Write(object threadId)
        {
            try
            {
                while (true)
                {
                    Console.WriteLine(_rwLock.CurrentReadCount + " concurrent readers");
                    int newNumber = GetRandNum(100);
                    _rwLock.EnterWriteLock();
                    _items.Add(newNumber);
                    _rwLock.ExitWriteLock();
                    Console.WriteLine("Thread " + threadId + " added " + newNumber);
                    Thread.Sleep(100);
                }
            }
            catch (ThreadAbortException)
            {
                Console.WriteLine("Thread aborted");
            }
        }

        private static int GetRandNum(int max)
        {
            lock (_random)
            {
                return _random.Next(max);
            }
        }
    }
}
﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class CompilerOptimizationExample : Example
    {
        bool _complete = false;

        public CompilerOptimizationExample()
        {
            Title = "Compiler optimization";
            Code = 402;
        }

        public override void Execute()
        {
            var t = new Thread(() =>
            {
                bool toggle = false;
                while (!_complete) // '_complete' value is cached.
                {
                    //Thread.MemoryBarrier();
                    //Console.WriteLine(toggle);
                    //Thread.Sleep(0);
                    toggle = !toggle;
                    
                }
            });
            t.Start();
            Console.WriteLine("Waiting thread to finish.");
            Thread.Sleep(1000);
            _complete = true;
            t.Join();
        }
    }
}
﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class CompilerReorderExample : Example
    {
        public CompilerReorderExample()
        {
            Title = "Compiler reorder";
            Code = 401;
        }

        public override void Execute()
        {
            while (true)
            {
                var foo = new Foo();
                var threadA = new Thread(foo.A);
                var threadB = new Thread(foo.B);

                threadA.Start();
                threadB.Start();
            }
        }

        private class Foo
        {
            private int _answer;
            private bool _complete;

            public void A()
            {
                _answer = 123;
                _complete = true;
            }

            public void B()
            {
                if (_complete)
                {
                    Console.WriteLine(_answer);
                }
                else
                {
                    Console.WriteLine("not complete");
                }
            }
        }
    }
}
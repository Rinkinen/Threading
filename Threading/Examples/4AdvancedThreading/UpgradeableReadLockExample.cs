﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Threading.Examples
{
    public class UpgradeableReadLockExample : Example
    {
        private static readonly ReaderWriterLockSlim RwLock = new ReaderWriterLockSlim();
        private static readonly List<int> Items = new List<int>();
        private static readonly Random Random = new Random();

        public UpgradeableReadLockExample()
        {
            Title = "Upgradeable read Lock";
            Code = 411;
        }

        public override void Execute()
        {
            var threads = new List<Thread>
            {
                new Thread(Read),
                new Thread(Read),
                new Thread(Read),
                new Thread(Read),
                new Thread(Read),
                new Thread(Read),
                new Thread(Write),
                new Thread(Remove)
            };

            threads.ForEach(t => t.Start());

            Console.WriteLine("Press [Enter] to abort threads.");
            Console.ReadLine();

            threads.ForEach(t => t.Abort());
        }

        private static void Read()
        {
            Thread.CurrentThread.Name = $"Read #{Thread.CurrentThread.ManagedThreadId}.";

            while (true)
            {
                RwLock.EnterReadLock();

                if (Items.Contains(123))
                {
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} read 123. Item count: {Items.Count}. Concurrent readers {RwLock.CurrentReadCount}.");
                }

                RwLock.ExitReadLock();
            }
        }

        private static void Write()
        {
            Thread.CurrentThread.Name = $"Write #{Thread.CurrentThread.ManagedThreadId}";

            while (true)
            {
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} getting upgradeable lock.");

                // Only one thread is allowed inside upgradeable lock. Readers can still
                RwLock.EnterUpgradeableReadLock();

                if (!Items.Contains(123))
                {
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} entering write lock for write.");
                    // If write locks are not used, then we allow concurrent readers while writing!
                    RwLock.EnterWriteLock();
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} got write lock.");

                    Items.Add(123);
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} added 123. Item count: {Items.Count}. Concurrent readers { RwLock.CurrentReadCount}.");

                    RwLock.ExitWriteLock();
                }

                RwLock.ExitUpgradeableReadLock();
            }
        }

        private void Remove()
        {
            Thread.CurrentThread.Name = $"Remove #{Thread.CurrentThread.ManagedThreadId}.";

            while (true)
            {
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} getting upgradeable lock.");

                RwLock.EnterUpgradeableReadLock();

                if (Items.Contains(123))
                {
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} entering write lock for remove.");
                    RwLock.EnterWriteLock();
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} got write lock.");

                    Items.Remove(123);
                    Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} removed 123. Item count: {Items.Count}. Concurrent readers {RwLock.CurrentReadCount}.");

                    RwLock.ExitWriteLock();
                }

                RwLock.ExitUpgradeableReadLock();
            }
        }
    }
}
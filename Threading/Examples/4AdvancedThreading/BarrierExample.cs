﻿using System;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class BarrierExample : Example
    {
        private static readonly Barrier Barrier = new Barrier(3, DoPostPhase);

        public BarrierExample()
        {
            Title = "Barrier";
            Code = 409;
        }

        public override void Execute()
        {
            var threads = Enumerable.Range(1, 3).Select(i => new Thread(Speak)).ToList();
            threads.ForEach(t => t.Start());
            threads.ForEach(t => t.Join());
        }

        private static void Speak()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.Write(i + " ");
                Barrier.SignalAndWait(100);
            }
        }

        /// <summary>
        /// Post-phase enables us to do something with phase values!
        /// </summary>
        private static void DoPostPhase(Barrier barrier)
        {
            Thread.Sleep(500);
            Console.WriteLine($"Phase {barrier.CurrentPhaseNumber} ended.");
        }
    }
}
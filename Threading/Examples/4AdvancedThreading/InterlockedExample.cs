﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class InterlockedExample : Example
    {
        private static long _sum;

        public InterlockedExample()
        {
            Title = "Interlocked";
            Code = 404;
        }

        public override void Execute()
        {
            // Simple increment/decrement operations:
            Interlocked.Increment(ref _sum);                             // Value 1
            Interlocked.Decrement(ref _sum);                             // Value 0

            // Add/subtract a value:
            Interlocked.Add(ref _sum, 3);                                // Value 3

            // Read a 64-bit field:
            Console.WriteLine(Interlocked.Read(ref _sum));               // Returns 3

            // Write a 64-bit field while reading previous value:
            // (This prints "3" while updating _sum to 10)
            Console.WriteLine(Interlocked.Exchange(ref _sum, 10));       // Value 10, returns 3

            // Update a field only if it matches a certain value (10):
            Console.WriteLine(Interlocked.CompareExchange(ref _sum, 123, 10));  // Value 123, returns 10
        }
    }
}
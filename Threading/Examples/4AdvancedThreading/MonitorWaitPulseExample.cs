﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class MonitorWaitPulseExample : Example
    {
        private static readonly object Locker = new object();
        private static bool _go;

        public MonitorWaitPulseExample()
        {
            Title = "Monitor.Wait/Pulse";
            Code = 406;
        }

        public override void Execute()
        {
            _go = false;
            new Thread(Work).Start();

            Console.WriteLine("Press [Enter] to wake up worker...");
            Console.ReadLine();            // Wait for user to hit Enter

            lock (Locker)                  // Let's now wake up the worker thread by setting _go=true and pulsing.
            {
                _go = true;
                Monitor.Pulse(Locker);
            }
        }

        private static void Work()
        {
            lock (Locker)
            {
                while (!_go)
                {
                    Monitor.Wait(Locker); // Lock is released while we’re waiting
                }
            }

            Console.WriteLine("Woken!!!");
        }
    }
}
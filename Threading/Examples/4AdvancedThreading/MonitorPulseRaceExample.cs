﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class MonitorPulseRaceExample : Example
    {
        private static readonly object Locker = new object();
        private static bool _workerReady, _workToBeDone;

        public MonitorPulseRaceExample()
        {
            Title = "Monitor.Pulse race";
            Code = 408;
        }

        public override void Execute()
        {
            new Thread(SaySomething).Start();

            for (int i = 0; i < 5; i++)
            {
                lock (Locker)
                {
                    // Uncomment for the logic to work.
                    //while (!_workerReady) Monitor.Wait(Locker);
                    _workerReady = false;
                    _workToBeDone = true;
                    Monitor.PulseAll(Locker);
                }
            }
        }

        private static void SaySomething()
        {
            for (int i = 0; i < 5; i++)
            {
                lock (Locker)
                {
                    _workerReady = true;
                    Monitor.PulseAll(Locker);

                    while (!_workToBeDone) Monitor.Wait(Locker);
                    _workToBeDone = false;
                    Console.WriteLine("Wassup?");
                }
            }
        }
    }
}
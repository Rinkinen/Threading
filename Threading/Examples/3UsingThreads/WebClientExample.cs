﻿using System;
using System.Net;
using System.Threading;

namespace Threading.Examples
{
    public class WebClientExample : Example
    {
        public WebClientExample()
        {
            Title = "WebClient";
            Code = 302;
        }

        public override void Execute()
        {
            var webClient = new WebClient();
            var workerReady = new ManualResetEventSlim(false);
            webClient.DownloadProgressChanged += WebClient_DownloadProgressChanged;
            webClient.DownloadDataCompleted += WebClient_DownloadDataCompleted;
            webClient.DownloadDataAsync(new Uri("http://ipv4.download.thinkbroadband.com/10MB.zip"), workerReady);

            Console.WriteLine("Waiting for download to finish...");
            workerReady.Wait();
            Console.WriteLine("File downloaded.");
        }

        private void WebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.WriteLine($"Downloading {e.ProgressPercentage}%...");
        }

        private void WebClient_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            ((ManualResetEventSlim)e.UserState).Set();
        }
    }
}
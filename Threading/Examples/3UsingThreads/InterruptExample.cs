﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class InterruptExample : Example

    {
        public InterruptExample()
        {
            Title = "Interrupt";
            Code = 303;
        }

        public override void Execute()
        {
            Thread t = new Thread(() =>
            {
                try
                {
                    Console.WriteLine("Sleeping forever.");
                    Thread.Sleep(Timeout.Infinite);
                    Console.WriteLine("We never get here!");
                }
                catch (ThreadInterruptedException)
                {
                    Console.Write("Forcibly ");
                }

                Console.WriteLine("Woken!");
            });
            t.Start();
            Console.WriteLine("Press [Enter] to interrupt the thread.");
            Console.ReadLine();
            t.Interrupt();
        }
    }
}
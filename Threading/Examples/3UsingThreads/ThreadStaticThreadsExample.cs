﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Threading.Examples
{
    public class ThreadStaticThreadsExample : Example
    {
        [ThreadStatic]
        private static int _localInt;

        public ThreadStaticThreadsExample()
        {
            Title = "ThreadStatic (Real threads)";
            Code = 309;
        }

        public override void Execute()
        {
            var threads = Enumerable.Range(1, 100).Select(i => new Thread(Increment)).ToList();
            threads.ForEach(t => t.Start());
            threads.ForEach(t => t.Join());

            Console.WriteLine($"Sum: {_localInt}");
        }

        private static void Increment()
        {
            for (int i = 0; i < 10000000; i++)
            {
                _localInt++;
            }

            Console.WriteLine($"Local int: {_localInt}");
        }
    }
}
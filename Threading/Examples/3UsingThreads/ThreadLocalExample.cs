﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ThreadLocalExample : Example
    {
        private readonly ThreadLocal<int> _localInt = new ThreadLocal<int>();

        public ThreadLocalExample()
        {
            Title = "ThreadLocal";
            Code = 310;
        }

        public override void Execute()
        {
            var tasks = Enumerable.Range(1, 100).Select(i => new Task(Increment)).ToList();
            tasks.ForEach(t => t.Start());
            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"Sum: {_localInt}");
        }

        private void Increment()
        {
            _localInt.Value = 0;

            for (int i = 0; i < 10000000; i++)
            {
                _localInt.Value++;
            }

            Console.WriteLine($"Local int: {_localInt}");
        }
    }
}
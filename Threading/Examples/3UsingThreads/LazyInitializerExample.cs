﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class LazyInitializerExample : Example
    {
        public LazyInitializerExample()
        {
            Title = "LazyInitializer";
            Code = 307;
        }

        public override void Execute()
        {
            PictureLibrary pictureLibrary = null;

            var locker = new object();
            var initialized = false;

            var tasks = Enumerable.Range(1, 20).Select(i => new Task(() =>
                {
                    try
                    {
                        LazyInitializer.EnsureInitialized(ref pictureLibrary, PictureLibrary.GetInstance);
                        //LazyInitializer.EnsureInitialized(ref pictureLibrary, ref initialized, ref locker, PictureLibrary.GetInstance);
                        Console.WriteLine($"Picture library hash: {pictureLibrary.GetHashCode()}. Number of pictures in library: {pictureLibrary.PictureCount}.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Exception in initialization: {ex.Message}");
                    }
                })).ToList();

            // Start all tasks simultaneously.
            foreach (var task in tasks)
            {
                task.Start();
            }

            Task.WaitAll(tasks.ToArray());
        }

        public class PictureLibrary
        {
            public bool Connected { get; private set; }

            public int PictureCount { get; private set; }

            // Hidden constructor.
            private PictureLibrary() { }

            /// <summary>
            /// Creates a new instance of <see cref="PictureLibrary"/>.
            /// </summary>
            /// <remarks>This method is not thread-safe!</remarks>
            /// <returns></returns>
            public static PictureLibrary GetInstance()
            {
                Console.WriteLine("Initializing PictureLibrary");

                var library = new PictureLibrary();

                // Connect to database.
                library.Connect();
                //throw new AuthenticationException("Password for database is not correct.");

                // Load pictures from database.
                library.LoadPictures();

                // Create thumbnails for all pictures in cache.
                library.CreateThumbnails();

                return library;
            }

            private void Connect()
            {
                Thread.Sleep(1000);
                Connected = true;
            }

            private void LoadPictures()
            {
                Thread.Sleep(1000);
                PictureCount = 124;
            }

            private void CreateThumbnails()
            {
                Thread.Sleep(1000);
            }
        }
    }
}
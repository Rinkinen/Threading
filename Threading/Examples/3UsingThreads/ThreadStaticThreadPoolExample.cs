﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ThreadStaticThreadPoolExample : Example
    {
        [ThreadStatic]
        private static int _localInt;

        public ThreadStaticThreadPoolExample()
        {
            Title = "ThreadStatic (Threadpool)";
            Code = 308;
        }

        public override void Execute()
        {
            var tasks = Enumerable.Range(1, 100).Select(i => new Task(Increment)).ToList();
            tasks.ForEach(t => t.Start());            
            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"Sum: {_localInt}");
            Console.WriteLine("These values seem a bit odd... don't they?");
        }

        private static void Increment()
        {
            _localInt = 0;

            for (int i = 0; i < 10000000; i++)
            {
                _localInt++;
            }

            Console.WriteLine($"Local int: {_localInt}");
        }
    }
}
﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class CancellationTokenExample : Example
    {
        private readonly ManualResetEventSlim _workDone = new ManualResetEventSlim(false);

        public CancellationTokenExample()
        {
            Title = "CancellationToken";
            Code = 305;
        }

        public override void Execute()
        {
            var cancelSource = new CancellationTokenSource();
            cancelSource.Token.Register(CancellationWasCalled);

            new Thread(() => Work(cancelSource.Token)).Start();

            Console.WriteLine("Press [Enter] to cancel the work.");
            Console.ReadLine();
            cancelSource.Cancel();

            // Wait until worker is finished.
            _workDone.Wait();

            Console.WriteLine("Work finished");
        }

        private void CancellationWasCalled()
        {
            Console.WriteLine("Cancellation was called.");
        }

        private void Work(CancellationToken cancelToken)
        {
            try
            {
                while (true)
                {
                    // Cancel work here before starting another work cycle.
                    cancelToken.ThrowIfCancellationRequested();

                    Console.WriteLine("Working 1...");
                    Thread.Sleep(1000);
                    Console.WriteLine("Working 2...");
                    Thread.Sleep(1000);
                    Console.WriteLine("Working 3...");
                    Thread.Sleep(1000);
                }
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Work was canceled.");
            }

            _workDone.Set();
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.Threading;

namespace Threading.Examples
{
    public class BackgroundWorkerExample : Example
    {
        private readonly ManualResetEventSlim _workerReady = new ManualResetEventSlim(false);

        public BackgroundWorkerExample()
        {
            Title = "BackgroundWorker";
            Code = 301;
        }

        public override void Execute()
        {
            var bgWorker = new BackgroundWorker { WorkerReportsProgress = true };

            bgWorker.ProgressChanged += _bgWorker_ProgressChanged;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            bgWorker.DoWork += BgWorker_DoWork;
            bgWorker.RunWorkerAsync();

            Console.WriteLine("Waiting background worker to finish...");
            _workerReady.Wait();
            Console.WriteLine("Work done.");
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _workerReady.Set();
        }

        private void _bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine($"Working {e.ProgressPercentage}%...");
        }

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var bgWorker = (BackgroundWorker)sender;

            for (var i = 1; i <= 10; i++)
            {
                // Simulate work.
                Thread.Sleep(1000);

                bgWorker.ReportProgress(i * 10);
            }
        }
    }
}
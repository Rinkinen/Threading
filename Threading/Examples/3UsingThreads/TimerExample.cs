﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class TimerExample : Example

    {
        public TimerExample()
        {
            Title = "Timer";
            Code = 312;
        }

        public override void Execute()
        {
            var timer = new Timer(Tick, null, 1000, 100);
            Console.WriteLine("Press [Enter] to stop timer...");
            Console.ReadLine();

            timer.Dispose();
        }

        private static void Tick(object data)
        {
            Console.WriteLine($"Doing stuff in thread {Thread.CurrentThread.ManagedThreadId}.");
            //Thread.Sleep(1000);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class LazyExample : Example
    {
        public LazyExample()
        {
            Title = "Lazy<T>";
            Code = 306;
        }

        public override void Execute()
        {
            // Locks are used to ensure that only a single thread can initialize a Lazy<T> instance in a thread-safe manner.
            //var lazyInitialize = new Lazy<PictureLibrary>(PictureLibrary.GetInstance);
            var lazyInitialize = new Lazy<PictureLibrary>(PictureLibrary.GetInstance, LazyThreadSafetyMode.ExecutionAndPublication);

            // The Lazy<T> instance is not thread safe; if the instance is accessed from multiple threads, its behavior is undefined.
            //var lazyInitialize = new Lazy<PictureLibrary>(PictureLibrary.GetInstance, LazyThreadSafetyMode.None);

            // When multiple threads try to initialize a Lazy<T> instance simultaneously, all threads are allowed to run the initialization method.
            //var lazyInitialize = new Lazy<PictureLibrary>(PictureLibrary.GetInstance, LazyThreadSafetyMode.PublicationOnly);

            var tasks = Enumerable.Range(1, 20).Select(i => new Task(() =>
            {
                try
                {
                    var pictureLibrary = lazyInitialize.Value;
                    Console.WriteLine($"Picture library hash: {pictureLibrary.GetHashCode()}. Number of pictures in library: {pictureLibrary.PictureCount}.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception in initialization: {ex.Message}");
                }
            })).ToList();

            // Start all tasks simultaneously.
            foreach (var task in tasks)
            {
                task.Start();
            }

            Task.WaitAll(tasks.ToArray());
        }

        public class PictureLibrary
        {
            public bool Connected { get; private set; }

            public int PictureCount { get; private set; }

            // Hidden constructor.
            private PictureLibrary() { }

            /// <summary>
            /// Creates a new instance of <see cref="PictureLibrary"/>.
            /// </summary>
            /// <remarks>This method is not thread-safe!</remarks>
            /// <returns></returns>
            public static PictureLibrary GetInstance()
            {
                Console.WriteLine("Initializing PictureLibrary");

                var library = new PictureLibrary();

                // Connect to database.
                library.Connect();
                //throw new AuthenticationException("Password for database is not correct.");

                // Load pictures from database.
                library.LoadPictures();

                // Create thumbnails for all pictures in cache.
                library.CreateThumbnails();

                return library;
            }

            private void Connect()
            {
                Thread.Sleep(1000);
                Connected = true;
            }

            private void LoadPictures()
            {
                Thread.Sleep(1000);
                PictureCount = 124;
            }

            private void CreateThumbnails()
            {
                Thread.Sleep(1000);
            }
        }
    }
}
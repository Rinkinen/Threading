﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class AbortExample : Example
    {
        public AbortExample()
        {
            Title = "Abort";
            Code = 304;
        }

        public override void Execute()
        {
            Thread t = new Thread(() =>
            {
                try
                {
                    try
                    {
                        Console.WriteLine("Sleeping forever.");
                        Thread.Sleep(Timeout.Infinite);
                        Console.WriteLine("We never get here!");
                    }
                    catch (ThreadAbortException)
                    {
                        //Thread.ResetAbort();
                        Console.WriteLine("Aborted once.");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Aborted twice.");
                }

                Console.WriteLine("We only get here if Thread.ResetAbort is called.");
            });
            t.Start();
            Console.WriteLine("Press [Enter] to abort the thread.");
            Console.ReadLine();
            t.Abort();
        }
    }
}
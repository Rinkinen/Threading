﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Threading.Examples
{
    public class LocalDataStoreSlotExample : Example
    {
        public LocalDataStoreSlotExample()
        {
            Title = "LocalDataStoreSlot";
            Code = 311;
        }

        public override void Execute()
        {
            var threads = new List<Thread>();

            for (int i = 0; i < 10; i++)
            {
                int localInt = i;
                var thread = new Thread(AccessDataStore);
                threads.Add(thread);
                thread.Start(localInt);
            }

            threads.ForEach(t => t.Join());
        }

        private void AccessDataStore(object value)
        {
            var slot = Thread.GetNamedDataSlot("slot1");
            Console.WriteLine($"Setting value '{value}' in thread {Thread.CurrentThread.ManagedThreadId}.");
            Thread.SetData(slot, value);

            Thread.Sleep(1000);

            var readValue = Thread.GetData(slot);
            Console.WriteLine($"Getting value '{readValue}' in thread {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Threading.Examples
{
    public class ExceptionHandlingExample : Example
    {
        public ExceptionHandlingExample()
        {
            Code = 110;
            Title = "Exception handling";
        }

        public override void Execute()
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                new Thread(Go).Start();
            }
            catch (Exception)
            {
                // We'll never get here!
                Console.WriteLine("Exception!");
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine($"Sender: {sender}.");
            Console.WriteLine($"e: {e}");
            Console.WriteLine($"e.ExceptionObject: {e.ExceptionObject}.");
            Debugger.Break();
        }

        private static void Go()
        { throw null; }   // Throws a NullReferenceException
    }
}
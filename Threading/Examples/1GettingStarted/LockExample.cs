﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class LockExample : Example
    {
        public LockExample()
        {
            Code = 105;
            Title = "Use locking";
        }

        public override void Execute()
        {
            ThreadTest.Execute();
        }

        private static class ThreadTest
        {
            private static bool _done;    // Static fields are shared between all threads
            private static readonly object Locker = new object();

            public static void Execute()
            {
                _done = false;
                new Thread(Go).Start();
                Go();
            }

            private static void Go()
            {
                // Lock to prevent simultaneous execution.
                lock (Locker)
                {
                    if (!_done)
                    {
                        Thread.Sleep(100);
                        _done = true;
                        Console.WriteLine("Done");
                    }
                }
            }
        }
    }
}
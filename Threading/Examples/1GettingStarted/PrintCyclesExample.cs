﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class PrintCyclesExample : Example
    {
        public PrintCyclesExample()
        {
            Code = 102;
            Title = "Print cycles";
        }

        public override void Execute()
        {
            new Thread(Go).Start();      // Call Go() on a new thread
            Go();                         // Call Go() on the main thread
        }

        private static void Go()
        {
            // Declare and use a local variable - 'cycles'
            for (int cycles = 0; cycles < 5; cycles++) Console.Write(cycles);
        }
    }
}
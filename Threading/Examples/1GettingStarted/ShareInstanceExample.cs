﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class ShareInstanceExample : Example
    {
        public ShareInstanceExample()
        {
            Code = 103;
            Title = "Share instance member";
        }

        public override void Execute()
        {
            ThreadTest.Execute();
        }

        private class ThreadTest
        {
            private bool _done;

            private ThreadTest()
            {
                _done = false;
            }

            public static void Execute()
            {
                ThreadTest tt = new ThreadTest();   // Create a common instance
                new Thread(tt.Go).Start();
                tt.Go();
            }

            // Note that Go is now an instance method
            private void Go()
            {                
                if (!_done)
                {
                    _done = true;
                    Console.WriteLine("Done");
                }
            }
        }
    }
}
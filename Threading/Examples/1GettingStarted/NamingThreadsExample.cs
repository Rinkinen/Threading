﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Threading.Examples
{
    public class NamingThreadsExample : Example
    {
        public NamingThreadsExample()
        {
            Code = 108;
            Title = "Naming threads";
        }

        public override void Execute()
        {
            // Name is not allowed to set twice.
            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "main";
            }
            Thread worker = new Thread(Go);
            worker.Name = "worker";
            worker.Start();

            Thread unnamed = new Thread(Go);
            unnamed.Start();

            Go();
        }

        private static void Go()
        {
            Console.WriteLine("Hello from " + Thread.CurrentThread.Name);

            // Thread's name can be viewed also in Threads window.
            Debugger.Break();
        }
    }
}
﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class JoinAndSleepExample : Example
    {
        public JoinAndSleepExample()
        {
            Code = 106;
            Title = "Join and Sleep";
        }

        public override void Execute()
        {
            Thread t = new Thread(Go);
            t.Start();
            t.Join();
            Console.WriteLine("Thread t has ended!");
        }

        private static void Go()
        {
            for (int i = 0; i < 1000; i++)
            {
                Console.Write("Y");
                // Makes printing to take longer, to demonstrate the wait in Join.
                Thread.Sleep(5);
            }
        }
    }
}
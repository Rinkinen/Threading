﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class WriteXAndYExample : Example
    {
        public WriteXAndYExample()
        {
            Code = 101;
            Title = "Write X and Y";
        }

        public override void Execute()
        {
            Thread t = new Thread(WriteY);          // Kick off a new thread
            t.Start();                               // running WriteY()

            // Simultaneously, do something on the main thread.
            for (int i = 0; i < 1000; i++)
            {
                Console.Write("X");
            }

            // If this is uncommented, the execution is joined to the main thread.
            //t.Join();
        }

        private static void WriteY()
        {
            for (int i = 0; i < 1000; i++)
            {
                Console.Write("Y");
            }
        }
    }
}
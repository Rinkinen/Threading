﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples._1GettingStarted
{
    public class ASync : Example
    {
        public ASync()
        {
            Title = "Async";
            Code = 100;
        }

        public override void Execute()
        {
            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "Main";
            }

            Console.WriteLine($"Main thread is {Thread.CurrentThread.ManagedThreadId}.");

            Task<string> google = DoLongRunningTaskWithoutThreads("https://www.google.fi/");
            Task<string> iltasanomat = DoLongRunningTaskWithoutThreads("https://www.is.fi/");
            Task<string> mtv = DoLongRunningTaskWithoutThreads("https://www.mtv.fi/");

            Task.WhenAll(google, iltasanomat, mtv).ContinueWith(results =>
            {
                foreach (var result in results.Result)
                {
                    Console.WriteLine($"result: {result.Substring(0, 50)}");
                }
            }).Wait();
        }

        private async Task<string> DoLongRunningTaskWithoutThreads(string url)
        {
            Console.WriteLine($"Running async method in thread {Thread.CurrentThread.ManagedThreadId}, with parameter: {url}");

            //await Task.Delay(3000);
            var webclient = new WebClient();
            var result = await webclient.DownloadStringTaskAsync(new Uri(url));

            return $"Results in thread {Thread.CurrentThread.ManagedThreadId}: {result}";
        }
    }
}
﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Threading.Examples
{
    public class ThreadPoolViaTplExample : Example
    {
        public ThreadPoolViaTplExample()
        {
            Code = 111;
            Title = "Entering the Thread Pool via TPL (Task Parallel Library)";
        }

        public override void Execute()
        {
            // Start the task executing:
            Task<string> task = Task.Run(() => DownloadString("http://www.linqpad.net"));

            // We can do other work here and it will execute in parallel:
            RunSomeOtherMethod(task);

            // When we need the task's return value, we query its Result property:
            // If it's still executing, the current thread will now block (wait)
            // until the task finishes:
            string result = task.Result.Substring(0, 1000);

            Console.WriteLine(result);
        }

        private void RunSomeOtherMethod(Task task)
        {
            // Work while web client is doing its job.
            while (!task.IsCompleted)
            {
                Console.WriteLine("Running some other method while web client is fetching data...");
                Thread.Sleep(50);
            }

            Console.WriteLine("Web client is ready.");
        }

        private static string DownloadString(string uri)
        {
            using (var wc = new System.Net.WebClient())
            {
                return wc.DownloadString(uri);
            }
        }
    }
}
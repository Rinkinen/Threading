﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class CapturedVariableExample : Example
    {
        public CapturedVariableExample()
        {
            Code = 107;
            Title = "Captured variables";
        }

        public override void Execute()
        {
            // Prints numbers in random order, possible same numbers multiple times.
            for (int index = 0; index < 10; index++)
            {
                // Anonymous functions are also supported.
                // Parameters can be delivered with Start-method.
                new Thread(() => Console.Write(index + ",")).Start();
            }

            Thread.Sleep(100);
            Console.WriteLine();

            // Prints numbers in random order.
            for (int index = 0; index < 10; index++)
            {
                // Anonymous functions are also supported.
                // Parameters can be delivered with Start-method.
                new Thread(i => Console.Write(i + ",")).Start(index);
            }

            Thread.Sleep(100);
            Console.WriteLine();

            // Prints numbers in random order.
            for (int index = 0; index < 10; index++)
            {
                int temp = index;
                // Parameters can be delivered with lambda expressions.
                new Thread(() => Console.Write(temp + ",")).Start();
            }
        }
    }
}
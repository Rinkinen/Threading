﻿using System;
using System.Threading;

namespace Threading.Examples
{
    public class ShareStaticMemberExample : Example
    {
        public ShareStaticMemberExample()
        {
            Code = 104;
            Title = "Share static member";
        }

        public override void Execute()
        {
            ThreadTest.Execute();
        }

        private static class ThreadTest
        {
            private static bool _done;    // Static fields are shared between all threads

            public static void Execute()
            {
                _done = false;
                new Thread(Go).Start();
                Go();
            }

            private static void Go()
            {
                if (!_done)
                {
                                                    //Thread.Sleep(100);                    
                    Console.WriteLine("Done");
                    _done = true;
                }
            }
        }
    }
}
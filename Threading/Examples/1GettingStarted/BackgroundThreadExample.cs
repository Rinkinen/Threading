﻿using System;

namespace Threading.Examples
{
    public class BackgroundThreadExample : Example
    {
        public BackgroundThreadExample()
        {
            Code = 109;
            Title = "Background thread";
        }

        public override void Execute()
        {
            Console.WriteLine("See the ForegroundBackgroundTest project.");
        }
    }
}
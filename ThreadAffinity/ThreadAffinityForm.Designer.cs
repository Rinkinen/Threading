﻿namespace ThreadAffinity
{
    partial class ThreadAffinityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdateFromUIThread = new System.Windows.Forms.Button();
            this.btnUpdateFromWorkerThreadBad = new System.Windows.Forms.Button();
            this.lblUpdateText = new System.Windows.Forms.Label();
            this.btnUpdateFromWorkerThreadGood = new System.Windows.Forms.Button();
            this.lblException = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnUpdateFromUIThread
            // 
            this.btnUpdateFromUIThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateFromUIThread.Location = new System.Drawing.Point(12, 12);
            this.btnUpdateFromUIThread.Name = "btnUpdateFromUIThread";
            this.btnUpdateFromUIThread.Size = new System.Drawing.Size(307, 29);
            this.btnUpdateFromUIThread.TabIndex = 0;
            this.btnUpdateFromUIThread.Text = "Update from UI thread";
            this.btnUpdateFromUIThread.UseVisualStyleBackColor = true;
            this.btnUpdateFromUIThread.Click += new System.EventHandler(this.btnUpdateFromUIThread_Click);
            // 
            // btnUpdateFromWorkerThreadBad
            // 
            this.btnUpdateFromWorkerThreadBad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateFromWorkerThreadBad.Location = new System.Drawing.Point(12, 47);
            this.btnUpdateFromWorkerThreadBad.Name = "btnUpdateFromWorkerThreadBad";
            this.btnUpdateFromWorkerThreadBad.Size = new System.Drawing.Size(307, 29);
            this.btnUpdateFromWorkerThreadBad.TabIndex = 1;
            this.btnUpdateFromWorkerThreadBad.Text = "Update from worker thread (bad)";
            this.btnUpdateFromWorkerThreadBad.UseVisualStyleBackColor = true;
            this.btnUpdateFromWorkerThreadBad.Click += new System.EventHandler(this.btnUpdateFromWorkerThreadBad_Click);
            // 
            // lblUpdateText
            // 
            this.lblUpdateText.AutoSize = true;
            this.lblUpdateText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateText.Location = new System.Drawing.Point(31, 149);
            this.lblUpdateText.Name = "lblUpdateText";
            this.lblUpdateText.Size = new System.Drawing.Size(268, 25);
            this.lblUpdateText.TabIndex = 2;
            this.lblUpdateText.Text = "This has not been updated";
            // 
            // btnUpdateFromWorkerThreadGood
            // 
            this.btnUpdateFromWorkerThreadGood.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateFromWorkerThreadGood.Location = new System.Drawing.Point(12, 82);
            this.btnUpdateFromWorkerThreadGood.Name = "btnUpdateFromWorkerThreadGood";
            this.btnUpdateFromWorkerThreadGood.Size = new System.Drawing.Size(307, 29);
            this.btnUpdateFromWorkerThreadGood.TabIndex = 3;
            this.btnUpdateFromWorkerThreadGood.Text = "Update from worker thread (good)";
            this.btnUpdateFromWorkerThreadGood.UseVisualStyleBackColor = true;
            this.btnUpdateFromWorkerThreadGood.Click += new System.EventHandler(this.btnUpdateFromWorkerThreadGood_Click);
            // 
            // lblException
            // 
            this.lblException.AutoSize = true;
            this.lblException.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblException.Location = new System.Drawing.Point(31, 174);
            this.lblException.Name = "lblException";
            this.lblException.Size = new System.Drawing.Size(0, 25);
            this.lblException.TabIndex = 4;
            // 
            // ThreadAffinityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 286);
            this.Controls.Add(this.lblException);
            this.Controls.Add(this.btnUpdateFromWorkerThreadGood);
            this.Controls.Add(this.lblUpdateText);
            this.Controls.Add(this.btnUpdateFromWorkerThreadBad);
            this.Controls.Add(this.btnUpdateFromUIThread);
            this.Name = "ThreadAffinityForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpdateFromUIThread;
        private System.Windows.Forms.Button btnUpdateFromWorkerThreadBad;
        private System.Windows.Forms.Label lblUpdateText;
        private System.Windows.Forms.Button btnUpdateFromWorkerThreadGood;
        private System.Windows.Forms.Label lblException;
    }
}


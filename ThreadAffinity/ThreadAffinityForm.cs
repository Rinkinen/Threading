﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadAffinity
{
    public partial class ThreadAffinityForm : Form
    {
        private int _counter;
        private readonly int _uiThreadId;

        public ThreadAffinityForm()
        {
            InitializeComponent();
            _uiThreadId = Thread.CurrentThread.ManagedThreadId;
        }

        private void UpdateAction()
        {
            string thread = Thread.CurrentThread.ManagedThreadId == _uiThreadId ? "UI" : "worker";
            _counter++;
            lblUpdateText.Text = $"Updated from {thread} thread : {_counter}";
        }

        private void btnUpdateFromUIThread_Click(object sender, EventArgs e)
        {
            lblException.Text = "";
            UpdateAction();
        }

        private void btnUpdateFromWorkerThreadBad_Click(object sender, EventArgs e)
        {
            try
            {
                lblException.Text = "";

                // Using real thread ends the application when the cross-thread usage occurs.
                new Thread(UpdateAction).Start();

                // Using ThreadPool, the application is not ended, when the cross-thread usage occurs.
                //var task = Task.Run(() => UpdateAction());

                //task.Wait();
                //if (task.Exception != null)
                //{
                //    MessageBox.Show(task.Exception.Message);
                //}
            }
            catch (AggregateException ex)
            {
                lblException.Text = ex.InnerException.Message;
            }
            catch (Exception ex)
            {
                lblException.Text = ex.Message;
            }
        }

        private void btnUpdateFromWorkerThreadGood_Click(object sender, EventArgs e)
        {
            lblException.Text = "";

            new Thread(() =>
            {
                if (lblUpdateText.InvokeRequired)
                {
                    lblUpdateText.Invoke((Action)UpdateAction);
                }
                else
                {
                    UpdateAction();
                }
            }).Start();
        }
    }
}
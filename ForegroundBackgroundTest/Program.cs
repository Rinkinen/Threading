﻿using System;
using System.Threading;

namespace ForegroundBackgroundTest
{
    internal class Program
    {
        private static void Main()
        {
            Thread worker = new Thread(() =>
            {
                try
                {
                    Console.ReadLine();
                }
                // Finally block is ignored in background threads when the last foreground thread is stopped.
                finally
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Cleaning up...");
                    Thread.Sleep(1000);
                    Console.WriteLine("Clean!");
                    Thread.Sleep(1000);
                }
            });
            // Change to true for demo purposes.
            worker.IsBackground = false;
            worker.Start();

            Console.WriteLine("Main thread exited.");
        }
    }
}